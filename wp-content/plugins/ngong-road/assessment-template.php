<?php ?>
<?php echo '<img src="' . $thumbnail['sizes']['medium'] . '" alt="' . htmlentities(get_the_title()) . '" class="child-portrait right-tnail" style="width:150px;">'; ?>
<h3>
          <?php echo $ass['Report_for_Year__c']; ?> Assessment Report for <?php echo $name; ?>
        </h3>
<p>
Date Last Modified: <?php echo date('D, d M Y H:i:s',strtotime($ass['LastModifiedDate'])); ?><br />
<?php
  if ($ass['RecordTypeId'] == '0120g000000USvxAAG') {
    echo 'Self-assessment prepared by Student';
?>
</p>

<div class="sectionTitle">
    <h2>
      SCHOOL REPORT:
    </h2>
  </div>
<div class="divTableRow">
  <div class="question">Name</div>
  <div class="answer"><?php echo $ass['School']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Location</div>
  <div class="answer"><?php echo $ass['School_Location__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Current Stage</div>
  <div class="answer"><?php echo $ass['Current_Stage__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Level</div>
  <div class="answer"><?php echo $ass['Level__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Current Course</div>
  <div class="answer"><?php echo $ass['Current_Course__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Planned Graduation Date</div>
  <div class="answer"><?php echo $ass['Planned_Graduation_Date__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Report Year / Term</div>
  <div class="answer"><?php echo $ass['Report_for_Year__c'] . " " . $ass['Report_for_Term__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">New in School Life</div>
  <div class="answer"><?php echo $ass['New_in_School_Life__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">View of School and Course</div>
  <div class="answer"><?php echo $ass['View_of_School_and_Course__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">School Transportation</div>
  <div class="answer"><?php echo $ass['School_Transportation__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Graduation Prerequisites</div>
  <div class="answer"><?php echo $ass['Graduation_Prerequisites__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Results to Date</div>
  <div class="answer"><?php echo $ass['Results_to_Date__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Results Feelings</div>
  <div class="answer"><?php echo $ass['Results_Feelings__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Current Challenges</div>
  <div class="answer"><?php echo $ass['Current_Challenges__c']; ?></div>
</div>



<div class="sectionTitle">
    <h2>
      HOME REPORT:
    </h2>
  </div>
<div class="divTableRow">
  <div class="question">Home Life Update</div>
  <div class="answer"><?php echo $ass['Term_Update__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Current Residence Location</div>
  <div class="answer"><?php echo $ass['Current_Residence_Location__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Household Residents</div>
  <div class="answer"><?php echo $ass['Household_Residents__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Rent</div>
  <div class="answer"><?php echo $ass['Rent__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Spare Time Activities</div>
  <div class="answer"><?php echo $ass['Spare_Time_Activities__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Employment</div>
  <div class="answer"><?php echo $ass['Employment__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Career Goals</div>
  <div class="answer"><?php echo $ass['Career_Goals__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Steps to Achieve Goals</div>
  <div class="answer"><?php echo $ass['Steps_to_Achieve_Goals__c']; ?></div>
</div>

<?php
  // END Self-Assessment
  } else if ($ass['RecordTypeId'] == '01270000000YSvbAAG') {
  // Begin PS-Assessment
  echo 'Post-Secondary Assessment Prepared by Case Worker: ' . $ass['Report_completed_by__c'];
?>

</p>

<div class="sectionTitle">
  <h2>
    CASE WORKER REPORT:
  </h2>
</div>
<div class="divTableRow">
<div class="question">Case Worker's Appraisal</div>
<div class="answer"><?php echo $ass['Case_Worker_s_Appraisal__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Academic Assessment</div>
<div class="answer"><?php echo $ass['How__c']; ?></div>
</div>
</div>
<div class="divTableRow">
<div class="question">Employment Prospects</div>
<div class="answer"><?php echo $ass['CW_view_of_Employment_Prospects__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Home Life Comments</div>
<div class="answer"><?php echo $ass['Home_Additional_Comments__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Student Financial Situation</div>
<div class="answer"><?php echo $ass['Student_financial_Situation__c']; ?></div>
</div>


<?php
  // END PS-Assessment
  } else {
    echo 'Prepared by Case Worker: ' . $ass['Report_completed_by__c'];
?>
</p>

  <div class="sectionTitle">
    <h2>
      HOME LIFE:
    </h2>
  </div>

<div class="divTableRow">
  <div class="sectionHeader">
    <h3>Guardian</h3>
  </div>
</div>

<div class="divTableRow">
  <div class="question">Name</div>
  <div class="answer"><?php echo $ass['G_First_Name__c'] . " " . $ass['G_Last_Name__c']; ?></div>
</div>

<div class="divTableRow">
  <div class="question">Relationship to Child</div>
  <div class="answer"><?php echo $ass['Relationship_to_Child__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="sectionHeader">
    <h3>Parents</h3>
  </div>
</div>
<div class="divTableRow">
  <div class="question">Mother Living at Home?</div>
  <div class="answer"><?php echo $ass['Mother_At_Home__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Mother Living?</div>
  <div class="answer"><?php echo $ass['Mother_Living__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Father Living at Home?</div>
  <div class="answer"><?php echo $ass['Father_at_Home__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Father Living?</div>
  <div class="answer"><?php echo $ass['Father_Living__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="sectionHeader">
  <h3>Life at Home</h3>
  </div>
</div>
<div class="divTableRow">
  <div class="question">Overview</div>
  <div class="answer"><?php echo $ass['Homelife_Overview__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Who Lives with this Child?</div>
  <div class="answer"><?php echo $ass['people_at_Home__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">First Home Visit Date</div>
  <div class="answer"><?php echo $ass['First_Home_Visit_Date__c']; ?></div>
</div>
<div class="divTableRow">
  <div class="question">Guardian's View of this Child</div>
  <div class="answer"><?php echo $ass['Guardian_s_view_of_the_Child__c'].$ass['Guardian_s_Assessment_of_Child__c']; ?> </div>
</div>
<div class="divTableRow">
  <div class="question">Guardian's View of Ngong Road Program</div>
  <div class="answer"><?php echo $ass['Guardian_s_view_of_Ngong_Road_Program__c'].$ass['Guardian_s_hope_for_child_s_future__c']; ?></div>
</div>
<?php if ( ! empty($ass['Family_Benefits_from_NRCA__c']) ) { ?>
  <div class="divTableRow">
  <div class="question">Family Benefits from NRCA</div>
  <div class="answer"><?php echo $ass['Family_Benefits_from_NRCA__c']; ?></div>
</div>
<?php } ?>
<div class="divTableRow">
  <div class="question">What is New at Home - First Visit</div>
  <div class="answer"><?php echo $ass['What_s_New__c']; ?></div>
</div>


  <div class="sectionTitle">
    <h2>
  SCHOOL LIFE:
    </h2>
  </div>

<div class="divTableRow">
  <div class="sectionHeader">
    <h3>Graduation Years:</h3>
  </div>
</div>
<div class="divTableRow">
<div class="question">Grammar School Class of:</div>
<div class="answer"><?php echo $grammar_class_of; ?></div>
</div>
<div class="divTableRow">
<div class="question">Secondary School Class of:</div>
<div class="answer"><?php echo $secondary_class_of; ?></div>
</div>
<div class="divTableRow">
<div class="sectionHeader">
<h3>School Visits</h3>
</div>
</div>
<div class="divTableRow">
<div class="question">First School Visit Date</div>
<div class="answer"><?php echo $ass['First_School_Visit_Date__c']; ?></div>
</div>
<?php if ( $ass['Second_School_Visit_Date__c'] != "" ) { ?>
<div class="divTableRow">
<div class="question">Second School Visit Date</div>
<div class="answer"><?php echo $ass['Second_School_Visit_Date__c']; ?></div>
</div>
<?php } ?>
<!-- Now displayed on dashboard
<div class="divTableRow">
<div class="question">School Marks:</div>
<div class="answer"><em> { This data coming soon! } </em></div>
</div>
-->
<div class="divTableRow">
<div class="sectionHeader"><h3>Teacher's View of this Child</h3></div>
</div>
<div class="divTableRow">
<div class="question">Overall Assessment</div>
<div class="answer"><?php echo $ass['Teacher_s_Assessment__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Scholastic Strengths</div>
<div class="answer"><?php echo $ass['Scholastic_Strengths__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Subjects most in need of improvement</div>
<div class="answer"><?php echo $ass['Subject_for_improvement__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Extra Curricular Activities</div>
<div class="answer"><?php echo $ass['Extra_Curricular_Activities__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Comments/Recommendations for Academic Future</div>
<div class="answer"><?php echo $ass['Focus_for_Improvement__c'].$ass['School_Adtnl_Cmnts__c']; ?></div>
</div>

<div class="divTableRow">
  <div class="sectionTitle">
    <h2>
  COMMUNITY LIFE:
   </h2>
  </div>
</div>

<div class="divTableRow">
<div class="sectionHeader">
<h3>Case Worker's Appraisal</h3>
</div>
</div>
<div class="divTableRow">
<div class="question">Program Event Attendance</div>
<div class="answer"><?php echo $ass['Program_Attendance__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Community Participation Observations</div>
<div class="answer"><?php echo $ass['Ngong_Community_Assessment__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Summary Observations</div>
<div class="answer"><?php echo $ass['Case_Worker_s_Appraisal__c']; ?></div>
</div>
<div class="divTableRow">
<div class="question">Child's Health Observations</div>
<div class="answer"><?php echo $ass['Child_s_Health_Assess__c']; ?></div>
</div>

<?php if ( ! empty ($ass['Child_s_View_of_NRCA_Program__c'] ) || ! empty ( $ass['How_does_child_feel_about_Program__c']) ) { ?>
<div class="divTableRow">
  <div class="sectionTitle">
    <h2>
  CHILD'S PERSPECTIVE:
   </h2>
  </div>
</div>
<?php } ?>
<?php if ( ! empty ($ass['Child_s_View_of_NRCA_Program__c'] ) || ! empty ( $ass['How_does_child_feel_about_Program__c']) ) { ?>
  <div class="divTableRow">
  <div class="question">Child's Feelings about NRCA Program</div>
  <div class="answer"><?php echo $ass['Child_s_View_of_NRCA_Program__c'].$ass['How_does_child_feel_about_Program__c']; ?></div>
</div>
<?php } ?>
<?php if ( ! empty ($ass['NRCA_Means_I_Can__c'] ) ) { ?>
  <div class="divTableRow">
  <div class="question">NRCA Means I Can</div>
  <div class="answer"><?php echo $ass['NRCA_Means_I_Can__c']; ?></div>
</div>
<?php } ?>

<?php } // END Case worker assessment (not self-assessment)  ?>

<div class="back"><input type="button" value="&laquo; BACK" onclick="history.back(-1)" /></div>
