<?php ?>

<h3>
          School Marks Report for <?php echo $name; ?>
        </h3>

<p>
<?php echo '<img src="' . $thumbnail['sizes']['medium'] . '" alt="' . htmlentities(get_the_title()) . '" class="child-portrait right-tnail">'; ?>

<strong>School:</strong> <?php echo $ass['School_lookup__c']; ?><br />
<strong>Class Level:</strong> <?php echo $ass['Class_Level_for_Marks__c']; ?><br />
<strong>Report Card Date:</strong> <?php echo $ass['Report_Card_Date__c']; ?><br />
<strong>Report Card for Term:</strong> <?php echo $ass['Marks_for_Period__c']; ?><br />
</p>

<p>
Kenya has a system of standardized testing reflected in these marks. We use these marks to determine a child’s progress in the basic academic skills and they eventually will play a large part in determining the kind of educational and career opportunities which will be available to the child. 
<a href="https://ngongroad.org/school-marks/">Read more about the Kenyan education system and the meaning of these school marks &raquo;</a>
</p>

<?php 
  if ( $ass['Marks_for_Period__c'] == 'KCSE' ) { 
    ?>
      <p>
      <strong>Agriculture</strong>: <?php echo $ass['Agriculture_KcSe__c']; ?><br />
      <strong>Biology:</strong>  <?php echo $ass['Biology_KcSe__c']; ?><br />
      <strong>Business Studies</strong>: <?php echo $ass['Business_Studies_KcSe__c']; ?><br />
      <strong>Chemistry:</strong>  <?php echo $ass['Chemistry_KcSe__c']; ?><br />
      <strong>CRE (Religion):</strong>  <?php echo $ass['CRE_Religion_KcSe__c']; ?><br />
      <strong>English:</strong>  <?php echo $ass['English_KcSe__c']; ?><br />
      <strong>Geography</strong>: <?php echo $ass['Geography_KcSe__c']; ?><br />
      <strong>History and Government</strong>: <?php echo $ass['History_Government_KcSe__c']; ?><br />
      <strong>Home Science</strong>: <?php echo $ass['Home_Science_KcSe__c']; ?><br />
      <strong>Kiswahili:</strong>  <?php echo $ass['Kiswahili_KcSe__c']; ?><br />
      <strong>Math:</strong>  <?php echo $ass['Math_KcSe__c']; ?><br />
      <strong>Physics</strong>: <?php echo $ass['Physics_KcSe__c']; ?><br />
      </p>
      <p>    
      <strong>Mean Grade: </strong> <?php echo $ass['Mean_Grade_KcSe__c']; ?>
      </p>
    <?php
  } else if ( strpos ( $ass['Class_Level_for_Marks__c'] , 'Form' ) === 0) { 
      echo '<p>
          Secondary school exams are very rigorous, are based on either 800 or 1100 total points, and have new and more subjects included in the curriculum than primary school exams.  If the score is blank, your student did not take that course this term.
          </p>';
    ?>
      <p>
      <strong>Total Marks:</strong>  <?php echo $ass['Total_Marks__c']; ?>/<?php echo $ass['Total_Marks_out_of__c']; ?> (<?php echo $ass['Total_Marks_pcnt__c']; ?>%)<br />
      <strong>Class Rank:</strong>  <?php echo $ass['Class_Rank__c']; ?> out of <?php echo $ass['Total_Students_in_Class__c']; ?><br /><br />
      
      <strong>Computer:</strong>  <?php echo $ass['Computer__c']; ?><br />
      <strong>English:</strong>  <?php echo $ass['English_Total__c']; ?><br />
      <strong>Kiswahili:</strong>  <?php echo $ass['Kswahili_Total__c']; ?><br />
      <strong>Math:</strong>  <?php echo $ass['Math__c']; ?><br />
      <strong>Biology:</strong>  <?php echo $ass['Biology__c']; ?><br />
      <strong>Physics:</strong>  <?php echo $ass['Physics__c']; ?><br />
      <strong>Chemistry:</strong>  <?php echo $ass['Chemistry__c']; ?><br />
      <strong>History and Government</strong>: <?php echo $ass['History_Government__c']; ?><br />
      <strong>Geography</strong>: <?php echo $ass['Geography__c']; ?><br />
      <strong>Religious Ed</strong>: <?php echo $ass['Religious_Ed__c']; ?><br />
      <strong>Business Studies</strong>: <?php echo $ass['Business_Studies__c']; ?><br />
      <strong>Agriculture</strong>: <?php echo $ass['Agriculture__c']; ?><br />
      <strong>Home Science</strong>: <?php echo $ass['Home_Science__c']; ?><br />
      </p>
    <?php
  } else if ( strpos ( $ass['Class_Level_for_Marks__c'] , 'Class' ) === 0) { 
     echo '<p>
            For Primary School marks, scores of over 300 are good, over 350 indicate real ability and scores over 400 are excellent. Kenya Comprehensive Primary Exam or KCPE marks are those for the critical test taken by 8th graders. The scores determine the quality and academic challenge of the secondary school the child will be able to attend. Scores of 250 were average across Kenya, over 300 are good and above 380 are excellent. If your Class Level field says KCPE, then the scores are from this vital test.
           </p>';
    ?>
      <p>
      <strong>Total Marks:</strong>  <?php echo $ass['Total_Marks__c']; ?>/<?php echo $ass['Total_Marks_out_of__c']; ?> (<?php echo $ass['Total_Marks_pcnt__c']; ?>%)<br />
      <strong>Class Rank:</strong>  <?php echo $ass['Class_Rank__c']; ?> out of <?php echo $ass['Total_Students_in_Class__c']; ?><br /><br />
      
      <strong>Computer:</strong>  <?php echo $ass['Computer__c']; ?><br />
      <strong>English:</strong>  <?php echo $ass['English_Total__c']; ?><br />
      <strong>Kiswahili:</strong>  <?php echo $ass['Kswahili_Total__c']; ?><br />
      <strong>Math:</strong>  <?php echo $ass['Math__c']; ?><br />
      <strong>Science:</strong>  <?php echo $ass['Science__c']; ?><br />
      <strong>Social Studies:</strong>  <?php echo $ass['Social_Studies__c']; ?><br />
    
      </p>
    <?php
  } 
?>

<p>
We hope you can find time to encourage your child to continue to work hard on their studies. View <a href="/sponsor/write-your-child/">instructions on how to send an email or postal encouragement letter &raquo;</a>  
</p>
<h3>
Thank you again for your faithful sponsorship support!
</h3>


<div class="back"><input type="button" value="&laquo; BACK" onclick="history.back(-1)" /></div>
