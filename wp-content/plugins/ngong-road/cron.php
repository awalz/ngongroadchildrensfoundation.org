#!/usr/bin/php
<?php

ob_start();

require_once(__DIR__ . '/../../../wp-load.php');

$sfs = SalesforceSync::get_instance();

/**
 * Tell SalesForceSync about our two cron jobs
*/
$child_mappings = ngong_get_mapping('children');

$sfs->add_cron('children',$child_mappings,'ngong_sync_children');

$sponsor_mappings = ngong_get_mapping('sponsors');

$sfs->add_cron('sponsors',$sponsor_mappings,'ngong_sync_sponsors');

$sfs->run_cron(null,true);

$sfs->handle_crons('children');

$sfs->handle_crons('sponsors');

print "Done!\n";

print date('Y-m-d h:i:s') . "\n";

$output = ob_get_clean();

if( php_sapi_name() === 'cli' ) {
	$output .= "Interface: Cron Job or direct on command line\n";
} else {
	$output .= "Interface: Called from web\n";
	$output .= print_r($_SERVER,true);
	print $output . "\n";
}

file_put_contents(__DIR__ . '/last_cron_run',$output);
