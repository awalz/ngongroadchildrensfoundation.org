<?php
/*
Plugin Name: Ngong Road Functionality
Plugin URI: #
Description:
Author: Cimbura.com (Nick Ciske)
Version: 1.0
Author URI: http://cimbura.com/
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Image and thumbnail settings
define ( 'FILEPATH_TO_IMAGES', '/srv/users/serverpilot/apps/ngongroad/public/images/portraits/');
define ( 'URL_TO_IMAGES', 'https://ngongroad.org/images/portraits/');

add_theme_support("aesop-component-styles", array("parallax", "image", "quote", "gallery", "content", "video", "audio", "collection", "chapter", "document", "character", "map", "timeline" ) );

add_image_size( 'highlight', 320, 320, true );

add_image_size( 'headshot', 300, 300, array('center','top') );

add_image_size( 'nrg-banner', 1200, 450, true );


// Gravity Forms settings
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );  // Alow field labels to be hidden


// Login page customization
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/Ngong-Road-Logo.png);
		height:96px;
		width:195px;
		background-size: 195px 96px;
		background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Friends of Ngong Road Sponsor Portal';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

add_filter( 'gettext', 'my_custom_gettext', 20, 3 );
function my_custom_gettext( $translated_text, $text, $domain ) {
    if ( 'wp-login.php' === $GLOBALS['pagenow'] ) {
        if ( "Username or Email Address" == $translated_text )
		$translated_text = __( 'Email address');
		if ( "Check your email for the confirmation link." == $translated_text )
		$translated_text = __( 'An email has been sent to the given email address. Please check your email and follow the instructions to create your password.' );
	else if ( "Please enter your username or email address. You will receive a link to create a new password via email." == $translated_text )
		$translated_text = __( 'Please enter the email address you use to communicate with Friends of Ngong Road and click the button.<br /><br />Next check your email inbox for instructions on creating/updating your password.' );

    }
    return $translated_text;
}

/**
 * Display parameter from url
 *
 * @return param value as string
 * @author Andy Walz 2/15/2018
 */
add_shortcode( 'display_url_param', 'get_url_param' );
function get_url_param( $atts ){

	$a = shortcode_atts( array(
		'param' => 'id'
	), $atts );

	if ( isset($_REQUEST[$a['param']]) ){
		return sanitize_text_field ( $_REQUEST[$a['param']] );
	}
}

// Customize password reset email
add_filter( 'wp_mail_from', 'wpse_new_mail_from' );
function wpse_new_mail_from( $old ) {
    return 'info@ngongroad.org'; // Edit it with your email address
}

add_filter('wp_mail_from_name', 'wpse_new_mail_from_name');
function wpse_new_mail_from_name( $old ) {
    return 'Friends of Ngong Road Sponsor Portal'; // Edit it with your/company name
}

add_filter( 'retrieve_password_title', 'my_custom_retrieve_password_title', 10, 3 );
function my_custom_retrieve_password_title( $title, $user_login, $user_data ) {
    return sprintf( __( "FoNR Sponsor Portal Account Notice for %s" ), $user_login );
}

add_filter( 'retrieve_password_message', 'my_custom_retrieve_password_message', 10, 4 );
function my_custom_retrieve_password_message( $message, $key, $user_login, $user_data ) {
	$message = __( 'Welcome to the Friends of Ngong Road Sponsor Portal!' ) . "\r\n\r\n";
    //$message = __( 'A password reset has been requested for an account with the following username:' ) . "\r\n\r\n";
    //$message .= $user_login . "\r\n\r\n";
    $message .= __( 'Please click on the link below to create or update your password. If you are unable to click the link, copy and paste it into your web browser.' ) . "\r\n\r\n";
    $message .= network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
    $message .= __( 'If you did not request a password or it is no longer required, please ignore this email.' ) . "\r\n\r\n";

    return $message;
}

// disable random password
add_filter( 'random_password', 'disable_random_password', 10, 2 );

function disable_random_password( $password ) {
    $action = isset( $_GET['action'] ) ? $_GET['action'] : '';
    if ( 'wp-login.php' === $GLOBALS['pagenow'] && ( 'rp' == $action  || 'resetpass' == $action ) ) {
        return '';
    }
    return $password;
}

add_action('init','add_post_types');
function add_post_types(){

	register_post_type('school',Array(
		'labels' => Array(
			'name' => 'Schools',
			'singular_name' => 'School',
		),
		'public' => TRUE,
		'menu_position' => 20,
		'capability_type' => 'page',
		'hierarchical' => TRUE,
	));

	register_post_type('children',Array(
		'labels' => Array(
			'name' => 'Children',
			'singular_name' => 'Child',
		),
		'public' => TRUE,
		'menu_position' => 20,
		'capability_type' => 'page',
		'hierarchical' => TRUE,
	));

	register_post_type('news',Array(
		'labels' => Array(
			'name' => 'Sponsor News',
			'singular_name' => 'Sponsor News',
		),
		'public' => TRUE,
		'menu_position' => 20,
		'capability_type' => 'post',
		'hierarchical' => TRUE,
	));

	register_post_type('donations',Array(
		'labels' => Array(
			'name' => 'Donations',
			'singular_name' => 'Donation',
		),
		'public' => TRUE,
		'menu_position' => 20,
		'capability_type' => 'post',
		'hierarchical' => FALSE,
		'supports' => FALSE,
	));

	add_post_type_support( 'school', 'genesis-layouts' );
}

// get rid of WordPress SEO metabox - adapted from http://wordpress.stackexchange.com/a/91184/2015
function prefix_remove_wp_seo_meta_box() {
	remove_meta_box( 'wpseo_meta', 'donations', 'normal' ); // change custom-post-type into the name of your custom post type
}
add_action( 'add_meta_boxes', 'prefix_remove_wp_seo_meta_box', 100000 );

// Primary grid of primary school kids on main sponsorship page
add_shortcode('children_gallery',function($atts){

	//$children = get_posts(Array('post_type' => 'children','orderby' => 'title','posts_per_page' => 100));
	$children = get_posts(Array(
		'post_type' => 'children',
		'orderby' => 'title',
		'posts_per_page' => 100,
		'meta_query' => array(
		                      'relation' => 'AND',
													array(
													  'key' => 'child_sponsored',
														'value'    => array( 'Waiting for Sponsor', 'Grant Sponsored' ),
														'compare' => 'IN',
													),
													array(
														  'key' => 'enrollment_type_current_year',
														  'value'    => array( 'Primary Boarding School', 'Primary Day School', 'Day School', 'Day_School' ),
															'compare' => 'IN',
														)
		),
	));
	$gallery = '<div class="childgallery">';
	$count = 0;

	foreach($children as $childMeta){
		// Add children if they have a bio
		if ($childMeta->post_content) {
			$gallery .= ngr_get_child_profile_box( $childMeta->ID, array( 'one-third','childgriditem' ) );
			$count++;
		}
	}

	if ( $count % 3 == 1 ) {
		$gallery .= '<div class="childprofile one-third childgriditem"></div><div class="childprofile one-third childgriditem"></div>';
	}
	if ( $count % 3 == 2 ) {
		$gallery .= '<div class="childprofile one-third childgriditem"></div>';
	}

	$gallery .= "</div>";

	return $gallery;
});

// Secondary school kids grid
add_shortcode('secondary_children_gallery',function($atts){

	//$children = get_posts(Array('post_type' => 'children','orderby' => 'title','posts_per_page' => 100));
	$children = get_posts(Array(
		'post_type' => 'children',
		'orderby' => 'title',
		'posts_per_page' => 100,
		'meta_query' => array(
		                      'relation' => 'AND',
													array(
													  'key' => 'child_sponsored',
														'value'    => array( 'Waiting for Sponsor', 'Grant Sponsored' ),
														'compare' => 'IN',
													),
													array(
														  'key' => 'enrollment_type_current_year',
														  'value'    => array( 'Secondary Boarding School', 'Secondary Day School', 'Boarding School', 'Boarding_School' ),
															'compare' => 'IN',
														)
		),
	));
	$gallery = '<div class="childgallery">';
	$count = 0;
	foreach($children as $childMeta){
		// Add children if they have a bio
		if ($childMeta->post_content) {
			$gallery .= ngr_get_child_profile_box( $childMeta->ID, array( 'one-third','childgriditem' ) );
			$count++;
		}
	}

	if ( $count % 3 == 1 ) {
		$gallery .= '<div class="childprofile one-third childgriditem child-placeholder"></div><div class="childprofile one-third childgriditem child-placeholder"></div>';
	}
	if ( $count % 3 == 2 ) {
		$gallery .= '<div class="childprofile one-third childgriditem child-placeholder"></div>';
	}

	$gallery .= "</div>";
	return $gallery;
});

// Post-Secondary Higher Ed kids grid
add_shortcode('highered_children_gallery',function($atts){

	//$children = get_posts(Array('post_type' => 'children','orderby' => 'title','posts_per_page' => 100));
	$children = get_posts(Array(
		'post_type' => 'children',
		'orderby' => 'title',
		'posts_per_page' => 100,
		'meta_query' => array(
		                      'relation' => 'AND',
													array(
													  'key' => 'child_sponsored',
														'value'    => array( 'Waiting for Sponsor', 'Grant Sponsored' ),
														'compare' => 'IN',
													),
													array(
														  'key' => 'enrollment_type_current_year',
														  'value'    => array( 'Post Secondary', 'Trade School' ),
															'compare' => 'IN',
														)
		),
	));
	$gallery = '<div class="childgallery">';
	$count = 0;
	foreach($children as $childMeta){
		// Add children if they have a bio
		if ($childMeta->post_content) {
			$gallery .= ngr_get_child_profile_box( $childMeta->ID, array( 'one-third','childgriditem' ) );
			$count++;
		}
	}

	if ( $count % 3 == 1 ) {
		$gallery .= '<div class="childprofile one-third childgriditem"></div><div class="childprofile one-third childgriditem"></div>';
	}
	if ( $count % 3 == 2 ) {
		$gallery .= '<div class="childprofile one-third childgriditem"></div>';
	}

	$gallery .= "</div>";
	return $gallery;
});

function ngr_get_child_profile_box( $child_id, $classes = array(), $interactive = true ){

	if ( empty($child_id) ){
		return "";
	}

	$year_in_sec = 31556926;
	$now = time();

	$child = get_post( $child_id );
	$dateString = get_field( 'birthday', $child_id );
	$birthdayRaw = strtotime( $dateString );
	$dateAdded = strtotime( get_field( 'created_date', $child_id ) );
	$enrollmentType = get_field( 'enrollment_type_current_year', $child_id );
	$headshot_id = get_field( 'headshot', $child_id );

	if ( !empty( $headshot_id ) ) {
		$a = 1;

		// Some are stored as attachment IDs, others as attachment references. Ooops. -- Michael Moore July 6, 2016
		if ( !is_numeric( $headshot_id ) ) {
			$headshot_id = $headshot_id['ID'];
		}

		$headshot_src = wp_get_attachment_image_src( $headshot_id, 'headshot' );

		if ( empty( $headshot_src ) ) {
			$headshot_url = '//ngongroad.org/wp-content/uploads/2016/07/icon-profile.png';
		} else {
			$headshot_url = current( $headshot_src );
		}
	} else {
		$headshot_url = '//ngongroad.org/wp-content/uploads/2016/07/icon-profile.png';
	}

	$name = htmlentities( $child->post_title );

	$link = '<a href="'. get_permalink( $child->ID ) .'">';

	$profile = '';

	if( $classes )
		array_map( 'sanitize_html_class', $classes );

	$profile .= '<div class="childprofile ' . implode( ' ', $classes ) . '">';
	$profile .= $link . '<img width="230px" src="' . esc_url( $headshot_url ) . '" title="' . esc_attr( $name ) . '"></a>';

	if( $interactive ){
		$profile .= '<span class="childname"><b>' . esc_url( $link ) . esc_html( $name ) . '</a></b></span>';
	}else{
		$profile .= '<span class="childname"><b>' . $name . '</b></span>';
	}

	$profile .= '<span class="childage">Age ' . floor( ( $now - $birthdayRaw ) / $year_in_sec ) . ' years</span>';

	$dayswaiting = floor ( ( $now - $dateAdded ) / ( 60 * 60 * 24 ) );
	$waiting = $dayswaiting <= 999 ? "Waiting $dayswaiting Days" : "Waiting >3 Years";

	if ( $dayswaiting > 0 && strtolower( $enrollmentType ) != "boarding school" && strtolower( $enrollmentType ) != "trade school" && strtolower( $enrollmentType ) != "post secondary" ){
		if ( isset( $_REQUEST['type'] ) && $_REQUEST['type'] == "Renewal" ) {
			// Do not show days waiting for renewals
		} else {
			$profile .= '<span class="childage">' . $waiting . '</span>';
		}
	}

	if( $interactive )
		$profile .= '<span class="brownlink">' . $link . 'Learn More'. '</a></span>';
	$profile .= '</div>';

	return $profile;
}

/* remove the form submit button if we don't have a child ID */
/*
add_filter( 'gform_submit_button','possibly_hide_sponsorship_submit',10,2);
function possibly_hide_sponsorship_submit($button,$form){
	if(!isset($form['cssClass']) || $form['cssClass'] !== 'sponsor_a_child'){
		return $button;
	}

	if(!isset($_GET['child_id'])){
		return FALSE;
	}

	return $button;
}
 */

function ngr_fix_https_in_urls($url){

	// print '<!-- '. "\n\n" .'ORIGINAL URL: ' . $url . "\n";

	if ( !filter_var($url, FILTER_VALIDATE_URL) ) {
		return '';
	}

	$origurl = $url;

	//Correct protocol for https connections
	list($protocol, $uri) = explode('://', $url, 2);

	if(is_ssl()) {
		if('http' == $protocol) {
			$protocol = 'https';
		}
	} else {
		if('https' == $protocol) {
			$protocol = 'http';
		}
	}

	$newurl = $protocol.'://'.$uri;

// 	print 'NEW URL: ' . $newurl . "\n\n-->";

	if ( !filter_var($newurl, FILTER_VALIDATE_URL) ) {
		return $origurl;
	} else {
		return $newurl;
	}
}

add_action( 'gform_post_submission', 'hide_sponsored_child', 10, 2 );
function hide_sponsored_child($entry,$form){

	if(!isset($form['cssClass']) || $form['cssClass'] !== 'sponsor_a_child'){
		return;
	}

	// child_id => $entry[1]
	// child_sponsored => field_560c166082dc5
	$res = update_field('field_560c166082dc5',1,$entry[1]);

	// This below was used for testing
	// $child = get_post($entry[1]);
	// $sponsored = get_field('child_sponsored',$child->ID);

}

add_filter('genesis_before_content','display_featured_image_as_banner',5);
function display_featured_image_as_banner(){

	if ( is_singular() && has_post_thumbnail()) {
		/**
		 * Conditionally add the post thumbnail to single post
		 */
		if ( ! function_exists( 'get_field' ) || ! get_field( 'hide_featured_image' ) ) {

			the_post_thumbnail( 'nrg-banner', array( 'class' => 'banner-image' ) );
			if ( get_field( 'banner_caption' ) ) {
				print '<h2 class="banner-caption">' . the_field( 'banner_caption' ) . '</h2>';
			}

		}

	}

}


add_filter( 'body_class', 'ngr_body_class' );

function ngr_body_class( $classes ) {

	if( has_post_thumbnail() )
		$classes[] = 'has-featured-image';

	return $classes;

}


/* Prepopulate Sponsorship Gravity Form */

add_shortcode( 'ngong_child_preview', 'ngong_child_preview_sc' );

function ngong_child_preview_sc(){

	//$child_name = ( empty($_GET['child_name']) ? '<!-- No child name found -->' : $_GET['child_name'] );
	// $type = ( empty($_GET['type']) ? '' : $_GET['type'] );

	$child_id = ngong_child_id_lookup();

	// if( $type == 'Renewal' ){
	// 	$html = '<div style="border: 4px solid rgb(0,102,51); background-color: rgba(0,0,0,0.1); border-radius: 5px; padding: 20px; margin-bottom:30px;"><span class="childname childrenewal"><b>Renewal:<br></b>' . esc_html( $child_name ) . '</span></div>';
	// 	return $html;
	// } else {
	return  ngr_get_child_profile_box( $child_id, array(), false );
	//}

}


add_filter( 'gform_field_value_child_id', 'ngong_child_id_lookup' );

function ngong_child_id_lookup(){

	if ( empty( $_GET['child_id'] ) ) {
		if ( empty( $_GET['child_nrca_id'] ) ){
			return false;
		} else {
			// Look up $post_id based on NRCA ID
			$args = array(
			             'post_type' => array ('children'),
								   'meta_query' => array(
								       array(
								           'key' => 'child_nrca_id',
								           'value' => $_GET['child_nrca_id'],
								           'compare' => '=',
								       )
								   )
			);
			$query = new WP_Query($args);

			if ( $query->have_posts() ) {
            $query->the_post();
            return get_the_ID();
      } else {
      	return false;
      }
		}
	} else {
		return absint( $_GET['child_id'] );
	}

}

add_filter( 'gform_pre_render_2', 'populate_amt_options' );

//Note: when changing choice values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
//add_filter( 'gform_pre_validation', 'populate_amt_options' );

//Note: when changing choice values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
//add_filter( 'gform_admin_pre_render', 'populate_amt_options' );

//Note: this will allow for the labels to be used during the submission process in case values are enabled
//add_filter( 'gform_pre_submission_filter', 'populate_amt_options' );
function populate_amt_options( $form ) {

    //only populating drop down for form id 5
    if ( $form['id'] != 2 ) {
       return $form;
    }

	if ( isset($_GET['type'])) {
		$type = $_GET['type'];
	} else {
		$type = "";
	}

    $requiredamt = floatval ( $_GET['req'] );
	$enrollment = str_replace("_", " ", $_GET['enrollment']);

    if ( $type == 'Renewal' ) {
	    $srid = $_GET['srid'];

	    // For Renewals, lookup current amt in SF
	    $sfs = SalesforceSync::get_instance();

		$query = "SELECT npe03__Amount__c FROM npe03__Recurring_Donation__c WHERE RD_Sponsor_Relationship__c = '" . $srid . "' LIMIT 1";

		$json = $sfs->rest_query($query);

		if ( $json ) {
			foreach($json['records'] as $record){
				$price = floatval ($record['npe03__Amount__c']);
			}
		}

		//TODO HANDLE EMPTY $price

	    //Creating item array.
	    $items = array();

	    //Adding current level value.
	    $items[] = array( 'text' => 'Current Level ($' . $price . '/year)', 'value' => 'Current Level ($' . $price . '/year)', 'price' => $price, 'isSelected' => true );

		$changemsg = "";
	    if ($requiredamt != $price) {
	    	$childname = $_GET['child_name'];
	    	$changemsg = esc_attr( $childname ) . " is currently in " . esc_attr( $enrollment ) . ". The annual cost required for this tuition support is $". esc_attr( $requiredamt ) . ". All contributions are valued, and your student remains in the program regardless of the amount of support you provide, but giving the amount required to fully fund their level of education is a great help.";

	    	$items[] = array( 'text' => $enrollment . ' ($' . $requiredamt . '/year)', 'value' => $enrollment . ' ($' . $requiredamt . '/year)', 'price' => $requiredamt, 'isSelected' => false );
	    }
	  } else {

	  	//Adding new level value.
	    $items[] = array( 'text' => $enrollment . ' ($' . $requiredamt . '/year)', 'value' => $enrollment . ' ($' . $requiredamt . '/year)', 'price' => $requiredamt, 'isSelected' => true );

	  }

    //Adding items to field id 9 (sponsorship level field).
    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 9 ) {
            $field->choices = $items;
        }

        if ( $field->id == 22 ) {
        		$field->content = $changemsg;
        }
    }

    return $form;
}



/**
 * Just a function to return the desired mapping
 * We need the mapping in a few places and don't
 * want to use globals and don't have an object,
 * so here we are
 */
function ngong_get_mapping($jobName){

	$jobs = array(
		'children' => array(
			'Child__c' => array(
				'fields' => array(
					'Child_s_Status__c',
					'Birthdate__c',
					'Child_Photo_High_Res__c',
					'Child_First_Name__c',
					'Child_Last_Name__c',
					'Biographical_Information__c',
					'NRCA_ID__c',
					'School_CY__c',
					'Id',
					'Class_Level_CY__c',
					'Current_Enrollment_Year__c',
					'Enrollment_Type_CY__c',
					'Secondary_Class_of__c',
					'Grammar_Class_of__c',
					'Child_Also_Known_As__c',
					'CreatedDate',
				)
			)
		),

		'sponsors' => array(
			'Sponsor_Relationship__c' => array(
				'fields' => array(
					'Sponsor_Name__c',
					'Sponsored_Child__c',
					'Sponsorship_Renewal_URL__c',
					'SRelationship_Status__c',
					'Renewal_Month_SR__c',
				)
			),
			'Contact' => array(
				'fields' => array(
					'Id',
					'Email',
					'FirstName',
					'LastName',
				)
			)
		)
	);

	return $jobs[$jobName];
}

/**
 * This function handles syncing children from the SalesForce CSV file
 */
function ngong_sync_children($file_collection){

	date_default_timezone_set('America/Chicago');
	print date('l jS \of F Y h:i:s A') . "\n";
	print "Syncing children now\n<br />";

	$mappings = ngong_get_mapping('children');

	foreach($file_collection as $table => $files){
		foreach($files as $file){

			$fh = fopen($file,'r');

			// Discard header. We'll use our mapping instead. (Is this a bad idea?)
			fgetcsv($fh);

			// Loop through all child rows in csv
			while(!feof($fh)){
				$vals = fgetcsv($fh);
				if(!$vals){
					continue;
				}

				$row = array_combine($mappings[$table]['fields'],$vals);

//				if ( $row['Child_Last_Name__c'] !== "Anod"){
//					continue;
//				}

				$fullname = $row['Child_First_Name__c'] . ' ' . $row['Child_Last_Name__c'];
				print "• " . $fullname . " synced.<br />";
				if(empty($row['Id'])){
					continue;
				}

				// Try to get the post by the SF UUID
				$posts = get_posts(array(
					'post_type' => 'children',
					'post_status' => 'any',
					'meta_query' => array(
						array(
							'key' => 'child_sfuuid',
							'value' => $row['Id'],
							'compare' => '=',
						)
					)
				));

				$child = array_shift($posts);

				// If we have multiple children with the same SF UUID, trash them
				// so that next time we won't
				foreach($posts as $post){
					wp_trash_post($post->ID);
				}

				// If not found and status is not Left out of Program or Deceased, then publish them
				if(is_null($child)){
					$child_id = wp_insert_post(
						array(
							'post_content' => $row['Biographical_Information__c'],
							'post_title' => $fullname,
							'post_type' => 'children',
							'post_status' => 'draft',
						));

					print "Created new child for {$row['Id']}\n";
					flush();

					if(!is_wp_error($child_id)){
						$child = get_post($child_id);
					}
				} else {
					print "Found existing child for {$row['Id']}\n";
					flush();
				}

				// If found, update all fields
				$child->post_title = $fullname;
				$child->post_content = $row['Biographical_Information__c'];

				update_field('field_57225a78520ec', /*child_sfuuid*/ $row['Id'], $child->ID);
				update_field('field_57225a86520ed', /*child_nrca_id*/ $row['NRCA_ID__c'], $child->ID);
				update_field('field_560c166082dc5', /*child_sponsored*/ $row['Child_s_Status__c'], $child->ID);
				update_field('field_56055cbb3cb09', /*birthday*/ $row['Birthdate__c'], $child->ID);
				update_field('field_589e21439e4ed', /*class_level_current_year*/ $row['Class_Level_CY__c'], $child->ID);
				update_field('field_589e216f9e4ee', /*curent_enrollment_year*/ $row['Current_Enrollment_Year__c'], $child->ID);
				update_field('field_589e21849e4ef', /*enrollment_type_current_year*/ $row['Enrollment_Type_CY__c'], $child->ID);
				update_field('field_589e219f9e4f0', /*secondary_class_of*/ $row['Secondary_Class_of__c'], $child->ID);
				update_field('field_589e222f9e4f1', /*grammar_class_of*/ $row['Grammar_Class_of__c'], $child->ID);
				update_field('field_5bbf9529ceb30', /*created_date*/ $row['CreatedDate'], $child->ID);
				update_field('field_5bbf9513ceb2f', /*nickname*/ $row['Child_Also_Known_As__c'], $child->ID);

				// child_school is a relationship so this not working
				update_field('field_572760f60e95e', /*child_School*/ $row['School_CY__c'], $child->ID);

				update_field('field_58e7135d376dc', /*current_school*/ $row['School_CY__c'], $child->ID);


				/**
				 * Images:
				 * - Check if headshot exists
				 *	- If it does, set the headshot mtime to the attachment creation/modified time
				 *	- Else use headshot mtime of -1
				 * - Check if file exists on disk
				 *	- If it does, get the file mtime
				 *	- Else use file mtime of -1
				 *
				 * - If file mtime > headshot mtime
				 *	- sideload photo and update headshot
				 *
				 */

				if ( !empty( $row['NRCA_ID__c'] ) ) {

					$headshot = get_field('headshot',$child->ID);
					if(!empty($headshot) && !empty( $headshot['modified'] ) ) {
						$headshot_mtime = strtotime( $headshot['modified'] );
					} else {
						$headshot_mtime = -1;
					}
					$filepathtoimages = '/srv/users/serverpilot/apps/ngongroad/public/images/portraits/';
					$file_on_disk = $filepathtoimages . $row['NRCA_ID__c'] . '.jpg';
					$file_url = URL_TO_IMAGES . $row['NRCA_ID__c'] . '.jpg';
					// print $file_on_disk . "<br />";
					if ( file_exists( $file_on_disk ) ) {
						// print "EXISTS!";
						require_once( ABSPATH . '/wp-admin/includes/image.php' );
						$meta = wp_read_image_metadata( $file_on_disk );

						if ( !empty( $meta ) && array_key_exists( 'created_timestamp', $meta ) ) {
							$file_mtime = $meta['created_timestamp'];
							if ( $file_mtime === '0' ) {
								$file_mtime = filemtime( $file_on_disk );
							}
						} else {
							$file_mtime = -1;
						}
						// print "  " . $file_mtime . " > " . $headshot_mtime . " = " . ( $file_mtime > $headshot_mtime ) . " <br /><br />";
					} else {
						$file_mtime = -1;
					}

					if ( $file_mtime > $headshot_mtime ) {
					//if ( file_exists( $file_on_disk ) ) {
						$attachment_id = ngong_import_photo($child, $file_url );

						if($attachment_id !== false){
							update_field('field_56055bbbe69a5' /*headshot*/,$attachment_id,$child->ID);
						}
					}
				} else {
					$a = 1;
				}

				$child->post_status = 'publish';

				wp_update_post($child);

				// If found, and status is Left out of Program or Deceased, then unpublish them
//				if(!is_null($child) && ($row['Child_s_Status__c'] == 'Left the Program' || $row['Child_s_Status__c'] == 'Deceased')){
//					wp_trash_post($child->ID);
//				}

			}

			fclose($fh);
		}
	}
	return true;
}

/**
 * This function is much longer than I'd like, but it does
 * everything that needs to be done to link sponsors from the SF CSV files
 */
function ngong_sync_sponsors($file_collection){

	print "Syncing sponsors now...<br />";

	$mappings = ngong_get_mapping('sponsors');

	// First we need to make a map of relavant SF Child UUIDs to Child Post IDs and the sponsor contact ID
	/*
	  array = array(
		'contact sf id' => array(
			'child uuid' => 'child post',
			'child2 uuid' => 'child2 post',
		),
		'contact2 sf id' => array(
			'child3 uuid' => 'child post',
			'child4 uuid' => 'child2 post',
		),
	  )
	 */

	$sponsor_relationships = $file_collection['Sponsor_Relationship__c'];
	$sponsor_child_map = array();
	$sponsor_abjured = array();
	foreach($sponsor_relationships as $file){
		$fh = fopen($file,'r');
		// Discard header. We'll use our mapping instead. (Is this a bad idea?)
		fgetcsv($fh);

		while(!feof($fh)){
			$vals = fgetcsv($fh);
			if(!$vals){
				continue;
			}
			$sponsor_relationship = array_combine($mappings['Sponsor_Relationship__c']['fields'],$vals);
			//$sponsor = array_combine($mappings['Contact']['fields'],$vals);

			if(empty($sponsor_relationship['Sponsored_Child__c'])){
				// No related child, bail
				continue;
			}

			// Get all matching child CPTs that already exists using the child's SalesForce UUID
			$posts = get_posts(array(
				'post_type' => 'children',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key' => 'child_sfuuid',
						'value' => $sponsor_relationship['Sponsored_Child__c'],
						'compare' => '=',
					)
				)
			));

			$child = array_shift($posts);

			// If we have multiple children with the same SF UUID, trash the extras so that next time we won't
			foreach($posts as $post){
				wp_trash_post($post->ID);
			}

			// Link children to sponsors but ignore abjured sponsors
			if( !empty($child) &&
			    $sponsor_relationship['SRelationship_Status__c'] != 'Abjured' &&
			    $sponsor_relationship['SRelationship_Status__c'] != 'Child Left Pgm' &&
			    $sponsor_relationship['SRelationship_Status__c'] != 'Child Left Program' ) {

					$sponsor_child_map[$sponsor_relationship['Sponsor_Name__c']][$sponsor_relationship['Sponsored_Child__c']] = $child->ID;
					//update_field('field_58ee9212863ec', /*renewal_link*/ $sponsor_relationship['Sponsorship_Renewal_URL__c'], $child->ID);
					update_field('field_59a252feb53e1', $sponsor_relationship['Renewal_Month_SR__c'], $child->ID);

			} else {

				if ( $sponsor_relationship['SRelationship_Status__c'] == 'Abjured' || $sponsor_relationship['SRelationship_Status__c'] == 'Child Left Pgm'
				     || $sponsor_relationship['SRelationship_Status__c'] == 'Child Left Program' ) {
					$sponsor_abjured[] = $sponsor_relationship['Sponsor_Name__c'];

					// Unset adjured sponsors on child record
					update_field( 'field_5a86550a15ef5', '', $child->ID );
					update_field( 'field_560c168182dc6', '', $child->ID );

				}
			}
		}

		fclose($fh);
	}


	// Then we'll loop through the sponsor relationships and for any of the
	// relationship IDs that exist in our array, we'll see if they have a user account
	// yet. If they don't, then we create one
	//
	// TOOD: Determine how to detect and unset sponsors when a sponsor stops sponsoring

	$sponsor_files = $file_collection['Contact'];
	foreach($sponsor_files as $file){
		$fh = fopen($file,'r');

		// Discard header. We'll use our mapping instead. (Is this a bad idea?)
		fgetcsv($fh);

		while(!feof($fh)){
			$vals = fgetcsv($fh);

			if(!$vals){
				continue;
			}

			$sponsor_relationship = array_combine($mappings['Contact']['fields'],$vals);

			if(empty($sponsor_relationship['Id'])){
				continue;  // Missing primary key so bail
			}

			if(!array_key_exists($sponsor_relationship['Id'],$sponsor_child_map)){
				continue; // This contact has no children so bail
			}

			// Check for existing WordPress user
			$users = get_users(array(
				'meta_query' => array(
					array(
						'key' => 'user_sf_uuid',
						'value' => $sponsor_relationship['Id'],
						'compare' => '=',
					)
				)
			));

			$cur_user = array_shift($users);

			// If there are any redundant users, delete them
			foreach($users as $user){
				wp_delete_user($user->ID,$cur_user->ID);
			}

			// Create user if does not yet exist
			if(empty($cur_user)){

				// Check existing by email, just in case there's already an account
				$cur_user = get_user_by('email',$sponsor_relationship['Email']);

				if(empty($cur_user)){
					$user_id = wp_create_user(
						$sponsor_relationship['FirstName'] . ' ' . $sponsor_relationship['LastName'],
						wp_generate_password(),
						$sponsor_relationship['Email']
					);

					$cur_user = get_user_by('ID', $user_id);
				}

				if(empty($cur_user)){
					continue; // ERROR. Now what?
				}

				update_field('user_sf_uuid',$sponsor_relationship['Id'],'user_' . $cur_user->ID);

				$cur_user->set_role('sponsor');
				wp_update_user($cur_user);
			} else {
				// TODO Update user email if need be
				// $user_id = wp_update_user( array( 'ID' => $user_id, 'user_url' => $website ) );
			}

			// With a sponsor account found or created, now we make sure that all children associated
			// with the sponsor have that sponsor set in the child's ACF field
			foreach($sponsor_child_map[$sponsor_relationship['Id']] as $child_uuid => $child_post_id){
				// update_field('field_560c166082dc5' /*child_sponsored*/, $sponsor_relationship['Child_s_Status__c'] !== 'Waiting for Sponsor', $child->ID);
				update_field('field_560c168182dc6', $cur_user->ID, $child_post_id);

				// Read existing sponsors
				$users = get_field('sponsors', $child_post_id, false);

				// Construct an array of sponsor id numbers
				$ids = [];
				$exists = false;

				if ( ! empty($users) ) {
					foreach ($users as $u){
						if ( gettype($u) == "array"){
							$ids[] = strval ( $u['ID'] );
							if ( $u['ID'] == $cur_user->ID ){
								$exists = true;
							}
						} else if ( gettype($u) == "string" || gettype($u) == "integer"){
							$ids[] = strval ( $u );
							if ( $u == $cur_user->ID ){
								$exists = true;
							}
						}
					}
				}


				// add the users ID to the array
				if ( ! $exists ) {
					$ids[] = strval ( $cur_user->ID );
				}

				// update the sponsors ACF relationship field (type users)
				update_field('field_5a86550a15ef5', $ids, $child_post_id);
			}
		}

		fclose($fh);
	}

	return true;
}

// http://www.htmlcenter.com/blog/wordpress-import-images-from-another-website/
function ngong_import_photo( $post, $url ) {
	if( empty( $post ) ) {
		return false;
	}

	if( !class_exists( 'WP_Http' ) ) {
		include_once( ABSPATH . WPINC. '/class-http.php' );
	}

	$photo = new WP_Http();
	$photo = $photo->request( $url );

	if(is_wp_error($photo)){
		return false;
	}

	if( $photo['response']['code'] != 200 ) {
		return false;
	}

	$attachment = wp_upload_bits( $post->post_title . substr($url,-4), null, $photo['body'], date("Y-m", strtotime( $photo['headers']['last-modified'] ) ) );
	if( !empty( $attachment['error'] ) ) {
		return false;
	}

	$filetype = wp_check_filetype( basename( $attachment['file'] ), null );

	$postinfo = array(
		'post_mime_type'	=> $filetype['type'],
		'post_title'		=> $post->post_title . ' photo',
		'post_content'		=> '',
		'post_status'		=> 'inherit',
	);

	$filename = $attachment['file'];
	$attach_id = wp_insert_attachment( $postinfo, $filename, $post->ID);

	return $attach_id;
}

/**
 * Shortcode to display sponsor portal login on sponsorships page
 */
add_shortcode('sponsor-portal-login',function($atts = array()){
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		$content = '<p>Welcome back '.$current_user->data->display_name.'!<br />You\'re already logged in.<br /><a href="/sponsor-portal/"><strong>Proceed to Sponsor Portal &raquo;</strong></a></p>';

	} else {
		$content = '<h4 class="widget-title widgettitle">Sponsor Portal Login:</h4>';
		$args = array(
			'echo' => false,
			'redirect' => site_url( '/sponsor-portal/'),
			'form_id' => 'loginform-custom',
			'label_username' => __( 'Email Address' ),

			//'label_password' => __( 'Password' ),
			//'label_remember' => __( 'Remember Me custom text' ),
			'label_log_in' => __( 'Log In &raquo;' ),
			'remember' => true
		);
		$content .= wp_login_form( $args );
		$content .= '<hr />';
		$content .= '<a href=' . wp_lostpassword_url( get_permalink() ) . ' title="Lost Password">Lost Your Password?</a> | ';
		$content .= '<a href=' . wp_lostpassword_url( get_permalink() ) . ' title="Lost Password">New User?</a>';
	}
	return $content;
});

/**
 * Shortcode wrapper to ngong_show_assessment_list
 */
add_shortcode('ngong_assessment_list',function($atts = array()){
	if(isset($atts['child_post_id'])){
		$child_post_id = $atts['child_post_id'];
	}else{
		$child_post_id = null;
	}

	print ngong_show_assessment_list($child_post_id);
});

/**
 * Generate the list of assessments for the specified child post, or the current child post if no ID is specified
 *
 * @param post id $child_post_id (optional)
 * @return An HTML <ul></ul>
 */
function ngong_show_assessment_list($child_post_id = null, $child = []){
	if(is_null($child_post_id)){
		global $post;
		$child_post_id = $post->ID;
	}

	$sf_child_uuid = get_field('child_sfuuid',$child_post_id);

	$sfs = SalesforceSync::get_instance();

	$fields = 'Id, A_Child_Link__c, Report_for_Year__c, Report_completed_by__c, Report_for_School_Type__c, LastModifiedDate, CreatedDate, RecordTypeId';
	$table = 'Assessment_Report__c';

	$query = "SELECT $fields FROM $table WHERE A_Child_Link__c='$sf_child_uuid' AND Report_for_Year__c < '" . date('Y') . "' ORDER BY CreatedDate DESC NULLS LAST";

	$json = $sfs->rest_query($query);

	if ( $json ) {
		$list = '<ul>';
		foreach($json['records'] as $record){
			$list .= '<li class="one_assessment_link" data-assessment_id="' . htmlentities($record['Id']) . '"><a href="/sponsor-portal/assessment?Id=' .
				$record['Id'] . '&child=' . $child_post_id . '">';

			$list .=  htmlentities($record['Report_for_Year__c']);
			if(!empty($record['Report_for_School_Type__c'])){
				$list .= ' (' . htmlentities($record['Report_for_School_Type__c']) . ')';
			}

			$list .= '</a>';

			if(!empty($record['Report_completed_by__c'])){
				$list .= ' -- Completed by ' . htmlentities($record['Report_completed_by__c']);
			} else if ($record['RecordTypeId'] == '0120g000000USvxAAG') {
				$list .= ' -- Self Assessment by Student';
			}

			$list .= '</li>';
		}
		$list .= '</ul>';

		return $list;
	}
};


/**
 * Fetch a single assessment from SalesForce by assessment ID
 */
function ngong_get_single_assessment($assessment_id){
	$sfs = SalesforceSync::get_instance();

	$fields = 'A_Child_Link__c,Case_Worker_s_Appraisal__c,Child_s_Health_Assess__c,Child_s_Hopes_for_Future__c,Child_s_View_of_NRCA_Program__c,Child_View_of_School__c,CreatedById,CreatedDate,CurrencyIsoCode,Current_Height__c,Current_Weight_kilos__c,CW_view_of_Employment_Prospects__c,Date_of_Last_Book_Read__c,Excessive_Absence_Comments__c,Extra_Curricular_Activities__c,Father_at_Home__c,Father_HIV_Status__c,Father_Living__c,Favorite_Books__c,First_Home_Visit_Date__c,First_School_Visit_Date__c,Focus_for_Improvement__c,Guardian_Health_Status__c,Guardian_s_view_of_Ngong_Road_Program__c,Guardian_s_view_of_the_Child__c,G_First_Name__c,G_Last_Name__c,Homelife_Overview__c,Home_Additional_Comments__c,How_does_child_feel_about_Program__c,How__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Last_Book_Read__c,Last_Year_s_Height__c,Last_Year_s_Weight_kilos__c,Mother_At_Home__c,Mother_HIV_Status__c,Mother_Living__c,My_friends__c,Name,Next_of_Kin_Phone__c,Next_of_Kin__c,Ngong_Community_Assessment__c,people_at_Home__c,Percent_gain_weight__c,Percent_growth_height__c,Program_Attendance__c,PS_Student_s_Self_Assessment__c,P_G_HIV_Status__c,P_G_Notes__c,P_G_Phone__c,P_G_Residence__c,RecordTypeId,Relationship_to_Child__c,Report_completed_by__c,Report_for_School_Type__c,Report_for_Year__c,Rural_Relative_Contact_Info__c,Scholastic_Strengths__c,School_Adtnl_Cmnts__c,Second_Home_Visit_Date__c,Second_School_Visit_Date__c,Sponsor_should_know_this_about_me__c,Student_financial_Situation__c,Subject_for_improvement__c,Suggested_Sponsor_Influence_Points__c,SystemModstamp,Teacher_s_Assessment__c,Third_School_Visit_Date__c,What_I_want_to_know_about_Sponsor__c,What_s_New_Second_Visit__c,What_s_New__c,RecordTypeId,School_Location__c,School__c';
	$table = 'Assessment_Report__c';

	$query = "SELECT $fields FROM $table WHERE Id='$assessment_id'";

	$json = $sfs->rest_query($query);

	if($json === false){
		return false;
	}

	if(empty($json['records'][0])){
		return false;
	}

	return $json['records'][0];
};
add_shortcode('ngong_get_single_assessment',function(){

	if ( empty( $_GET['Id'] ) ) {
		return "This page will only work if an assessment ID is given as a parameter";
	}

	$assessmentId = $_GET['Id'];

	$sfs = SalesforceSync::get_instance();

	//$fields = 'A_Child_Link__c,Case_Worker_s_Appraisal__c,Child_s_Health_Assess__c,Child_s_Hopes_for_Future__c,Child_s_View_of_NRCA_Program__c,Child_View_of_School__c,CreatedById,CreatedDate,CurrencyIsoCode,Current_Height__c,Current_Weight_kilos__c,CW_view_of_Employment_Prospects__c,Date_of_Last_Book_Read__c,Excessive_Absence_Comments__c,Extra_Curricular_Activities__c,Family_Benefits_from_NRCA__c,Father_at_Home__c,Father_HIV_Status__c,Father_Living__c,Favorite_Books__c,First_Home_Visit_Date__c,First_School_Visit_Date__c,Focus_for_Improvement__c,Guardian_Health_Status__c,Guardian_s_Assessment_of_Child__c,Guardian_s_hope_for_child_s_future__c,Guardian_s_view_of_Ngong_Road_Program__c,Guardian_s_view_of_the_Child__c,G_First_Name__c,G_Last_Name__c,Homelife_Overview__c,Home_Additional_Comments__c,How_does_child_feel_about_Program__c,How__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Last_Book_Read__c,Last_Year_s_Height__c,Last_Year_s_Weight_kilos__c,Mother_At_Home__c,Mother_HIV_Status__c,Mother_Living__c,My_friends__c,Name,Next_of_Kin_Phone__c,Next_of_Kin__c,Ngong_Community_Assessment__c,NRCA_Means_I_Can__c,people_at_Home__c,Percent_gain_weight__c,Percent_growth_height__c,Program_Attendance__c,PS_Student_s_Self_Assessment__c,P_G_HIV_Status__c,P_G_Notes__c,P_G_Phone__c,P_G_Residence__c,RecordTypeId,Relationship_to_Child__c,Report_completed_by__c,Report_for_School_Type__c,Report_for_Year__c,Rural_Relative_Contact_Info__c,Scholastic_Strengths__c,School_Adtnl_Cmnts__c,Secondary_Student_Camp_Essay__c,Second_Home_Visit_Date__c,Second_School_Visit_Date__c,Sponsor_should_know_this_about_me__c,Student_financial_Situation__c,Subject_for_improvement__c,Suggested_Sponsor_Influence_Points__c,SystemModstamp,Teacher_s_Assessment__c,Third_School_Visit_Date__c,What_I_want_to_know_about_Sponsor__c,What_s_New_Second_Visit__c,What_s_New__c,School_Location__c,School_Transportation__c,School__c';
	$fields = 'A_Child_Link__c,School_Name__c,Career_Goals__c,Case_Worker_s_Appraisal__c,Child_s_Health_Assess__c,Child_s_Hopes_for_Future__c,Child_s_View_of_NRCA_Program__c,Child_View_of_School__c,CreatedById,CreatedDate,CurrencyIsoCode,Current_Challenges__c,Current_Course__c,Current_Height__c,Current_Residence_Location__c,Current_Stage__c,Current_Weight_kilos__c,CW_view_of_Employment_Prospects__c,Date_of_Last_Book_Read__c,Employment__c,Excessive_Absence_Comments__c,Extra_Curricular_Activities__c,Family_Benefits_from_NRCA__c,Father_at_Home__c,Father_HIV_Status__c,Father_Living__c,Favorite_Books__c,First_Home_Visit_Date__c,First_School_Visit_Date__c,Focus_for_Improvement__c,Graduation_Prerequisites__c,Guardian_Health_Status__c,Guardian_s_Assessment_of_Child__c,Guardian_s_hope_for_child_s_future__c,Guardian_s_view_of_Ngong_Road_Program__c,Guardian_s_view_of_the_Child__c,G_First_Name__c,G_Last_Name__c,Homelife_Overview__c,Home_Additional_Comments__c,Household_Residents__c,How_does_child_feel_about_Program__c,How__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Last_Book_Read__c,Last_Year_s_Height__c,Last_Year_s_Weight_kilos__c,Level__c,Mother_At_Home__c,Mother_HIV_Status__c,Mother_Living__c,My_friends__c,Name,New_in_School_Life__c,Next_of_Kin_Phone__c,Next_of_Kin__c,Ngong_Community_Assessment__c,NRCA_ID__c,people_at_Home__c,Percent_gain_weight__c,Percent_growth_height__c,Planned_Graduation_Date__c,Program_Attendance__c,PS_Student_s_Self_Assessment__c,P_G_HIV_Status__c,P_G_Notes__c,P_G_Phone__c,P_G_Residence__c,RecordTypeId,Relationship_to_Child__c,Rent__c,Report_completed_by__c,Report_for_School_Type__c,Report_for_Term__c,Report_for_Year__c,Results_Feelings__c,Results_to_Date__c,Rural_Relative_Contact_Info__c,Scholastic_Strengths__c,School_Adtnl_Cmnts__c,School_Location__c,School_Transportation__c,School__c,Secondary_Grad_Year__c,Secondary_Student_Camp_Essay__c,Second_School_Visit_Date__c,Spare_Time_Activities__c,Sponsor_should_know_this_about_me__c,Steps_to_Achieve_Goals__c,Student_financial_Situation__c,Subject_for_improvement__c,Suggested_Sponsor_Influence_Points__c,SystemModstamp,Teacher_s_Assessment__c,Term_Update__c,Third_School_Visit_Date__c,View_of_School_and_Course__c,What_I_want_to_know_about_Sponsor__c,What_s_New__c';
	$table = 'Assessment_Report__c';

	$query = "SELECT $fields FROM $table WHERE Id='{$_GET['Id']}'";

	$json = $sfs->rest_query($query);

	$html = '<!-- ';
	if ( isset ($_REQUEST['debug'])){
		$html = '<a href="https://na64.salesforce.com/'.$_REQUEST['Id'].'" target="_blank">View Assessment in SF &raquo;</a>';
	}

	if ( $json ) {
		if ( count($json['records']) > 0 ) {
			$html .= '<pre>';
			$html .= print_r($json['records'][0],true);
			$html .= '</pre>';
		}
	} else {
		$html = "That Assessment ID was not found.";
	}
	$ass = $json['records'][0];

	if ( $ass['School_Name__c'] !== null ) {
		$ass['School'] = $ass['School_Name__c'];
	} elseif ( $ass['School__c'] !== null ) {
		// Lookup school name
		$schoolquery = "SELECT Name FROM School__c WHERE Id = '" . $ass['School__c'] . "'";
		$school = $sfs->rest_query($schoolquery);
		$ass['School'] = $school['records'][0]['Name'];
	}

	$child_id = $_GET['child'];
	$child_post = get_post($child_id);
	$name = $child_post->post_title;
	$grammar_class_of = get_field( 'grammar_class_of', $child_id );
	$secondary_class_of = get_field( 'secondary_class_of', $child_id );
	$thumbnail = get_field('headshot', $child_id);
	$pic = $thumbnail['sizes']['medium'];

	include_once ( 'assessment-template.php');

	return $html . ' -->';
});

/**
 * Generate the list of assessments for the specified child post, or the current child post if no ID is specified
 *
 * @param post id $child_post_id (optional)
 * @return An HTML <ul></ul>
 */
 function ngong_show_school_marks_list($child_post_id = null, $child = []){
	if(is_null($child_post_id)){
		global $post;
		$child_post_id = $post->ID;
	}

	$sf_child_uuid = get_field('child_sfuuid',$child_post_id);

	$sfs = SalesforceSync::get_instance();

	$fields = 'Id,Class_Level_for_Marks__c,Marks_for_Child__c,Marks_for_Period__c,Name,RecordTypeId,Report_Card_Date__c,Marks_for_Year__c';
	$table = 'School_Marks__c';

	$query = "SELECT $fields FROM $table WHERE Marks_for_Child__c='$sf_child_uuid' ORDER BY Report_Card_Date__c DESC";

	$json = $sfs->rest_query($query);

	if ( $json ) {
		$list = '<ul>';
		$year = "";
		foreach($json['records'] as $record){
			if ( $year != $record['Marks_for_Year__c']) $list .= '<li>'.$record['Marks_for_Year__c'].'</li>';
			$year = $record['Marks_for_Year__c'];
			$list .= '<li class="one_schoolmarks_link" data-schoolmarks_id="' . htmlentities($record['Id']) . '"><a href="/sponsor-portal/school-marks/?Id=' .
			$record['Id'] . '&child=' . $child_post_id . '">';

			$list .=  htmlentities($record['Class_Level_for_Marks__c']);
			if(!empty($record['Marks_for_Period__c'])){
				$list .= ' (' . htmlentities($record['Marks_for_Period__c']) . ')';
			}

			$list .= '</a>';

			$list .= '</li>';
		}
		$list .= '</ul>';

		return $list;
	}
};

/**
 * Fetch a single assessment from SalesForce by assessment ID
 */
function ngong_get_single_school_marks(){
	if ( empty( $_GET['Id'] ) ) {
		return "This page will only work if an schoolmarks ID is given as a parameter";
	}

	$schoolmarksId = $_GET['Id'];

	$sfs = SalesforceSync::get_instance();

	$fields = 'Agriculture_KcSe__c,Agriculture__c,Biology_KcSe__c,Biology__c,Business_KcSe__c,Business_Studies__c,Chemistry_KcSe__c,Chemistry__c,Class_Level_for_Marks__c,Class_Rank__c,Computer__c,CreatedById,CreatedDate,CRE_Religion_KcSe__c,CurrencyIsoCode,English_KcSe__c,English_Total__c,Geography_KcSe__c,Geography__c,History_Government__c,History_KcSe__c,Home_Science_KcSe__c,Home_Science__c,Id,IsDeleted,Kiswahili_KcSe__c,Kswahili_Total__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Marks_for_Child__c,Marks_for_Period__c,Marks_for_Year__c,Marks_type_lookup__c,Math_KcSe__c,Math__c,Mean_Grade_KcSe__c,Name,OwnerId,Physics_KcSe__c,Physics__c,RecordTypeId,Religious_Ed__c,Report_Card_Date__c,School_lookup__c,Science__c,Social_Studies__c,SystemModstamp,Total_Marks_out_of__c,Total_Marks_pcnt__c,Total_Marks__c,Total_Students_in_Class__c';
	$table = 'School_Marks__c';

	$query = "SELECT $fields FROM $table WHERE Id='{$_GET['Id']}'";

	$json = $sfs->rest_query($query);

	$html = '<!-- ';

	if ( $json ) {
		if ( count($json['records']) > 0 ) {
			$html .= '<pre>';
			$html .= print_r($json['records'][0],true);
			$html .= '</pre> -->';
		}
	} else {
		$html = "That schoolmarks ID was not found.";
	}
	$ass = $json['records'][0];

	$child_id = $_GET['child'];
	$child_post = get_post($child_id);
	$name = $child_post->post_title;
	$grammar_class_of = get_field( 'grammar_class_of', $child_id );
	$secondary_class_of = get_field( 'secondary_class_of', $child_id );
	$thumbnail = get_field('headshot', $child_id);
	$pic = $thumbnail['sizes']['medium'];

	include_once ( 'schoolmarks-template.php');

	return $html;
};

add_shortcode('ngong_get_school_marks_report', 'ngong_get_single_school_marks');



add_filter ('gform_pre_render_2',function( $form ) {

	foreach( $form['fields'] as &$field ) {
		if ( !empty( $field->description ) ) {
			$field->description = do_shortcode($field->description);
		}
	}

	if(!isset($_GET['child_id'])){
		return $form;
	}

	$child_id = $_GET['child_id'];
	$child_post = get_post($child_id);
	$_GET['child_name'] = $child_post->post_title;

	return $form;
});

add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars ) {
	$qvars[] = 'child_id';
	$qvars[] = 'child_name';
	$qvars[] = 'type';
	return $qvars;
}

function nr_saveback_photo( $post_id, $post_after, $post_before ) {
	if ( $post_after->post_type === 'children' ) {
		$photo_id = get_field( 'headshot', $post_id, false );
		if ( !empty( $photo_id ) ) {
			$photo_path = get_attached_file( $photo_id );
			$nrca_id = get_field( 'child_nrca_id' );
			if ( !empty( $nrca_id ) ) {
				$filetype = wp_check_filetype( $photo_path );
				if ( $filetype['type'] === 'image/jpeg' ) {
					$new_file = FILEPATH_TO_IMAGES . $nrca_id . '.jpg';
					copy( $photo_path, $new_file );
				}
			}
		}
	}
}
add_action( 'post_updated', 'nr_saveback_photo', 10, 3 );

add_shortcode('ngong_get_school',function(){

		if ( empty( $_GET['Id'] ) ) {
			return "This page will only work if an school ID is given as a parameter";
		}

		$schoolID = $_GET['Id'];

		$sfs = SalesforceSync::get_instance();

		$fields = 'Name,NgongRoad_org_URL__c,School_Address__c,School_Category__c,School_Description__c,School_Level__c,School_Location__c,School_Location__Latitude__s,School_Location__Longitude__s,School_Notes_and_Information__c,School_Type__c,School_Webpage__c';
		$query = "SELECT " . $fields . " FROM School__c WHERE Id='{$_GET['Id']}'";

		$json = $sfs->rest_query($query);

		$html = '<!-- ';

		if ( $json ) {
			if ( count($json['records']) > 0 ) {
				$html .= '<pre>';
				$html .= print_r($json['records'][0],true);
				$html .= '</pre>';
			}
		} else {
			$html = "That School ID was not found.";
		}
		$school = $json['records'][0];

		// $child_id = $_GET['child'];
		// $child_post = get_post($child_id);
		// $name = $child_post->post_title;
		// $grammar_class_of = get_field( 'grammar_class_of', $child_id );
		// $secondary_class_of = get_field( 'secondary_class_of', $child_id );
		// $thumbnail = get_field('headshot', $child_id);
		// $pic = $thumbnail['sizes']['medium'];

		include_once ( 'school-template.php');

		return $html . ' -->';
	});

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function nr_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) && in_array( 'sponsor', $user->roles ) ) {
		return get_site_url() . '/sponsor-portal/';
	}
	return $redirect_to;
}

add_filter( 'login_redirect', 'nr_login_redirect', 10, 3 );

add_action('after_setup_theme', 'nr_remove_admin_bar');
function nr_remove_admin_bar() {
	if (
		!current_user_can('administrator') &&
		!current_user_can('editor') &&
		!current_user_can('accountant')
	) {
		show_admin_bar(false);
	}
}

add_shortcode('nr_child_school_type', function(){
	$child_id = $_GET['child_id'];
});

add_filter ( "acf/load_value/name=headshot", function( $value, $post_id, $field ){
	if ( empty( $value ) ) {
		$value = '3170'; // https://ngongroad.org/wp-content/uploads/2016/07/icon-profile.png
		$field = acf_get_field( 'field_56055bbbe69a5' );
		$value = acf_format_value( $value, $post_id, $field );
	}
	return $value;
},10,3);

/**
 * Get working leaflet map using leaflet-php lib
 *
 * @return HTML and JavaScript for working Leaflet map
 * @author Andy Walz 9/15/2016
 */
 function nr_get_map ( $divid = NULL, $cssclass = NULL, $lat = 0, $long = 0, $zoom = 5, $geojson = NULL ) {
	require_once 'lib/leaflet-php/leaflet-php-loader.php';

	$map = new LeafletPHP(array(
  		'center' => [$lat, $long],
		  'zoom' => $zoom,
	),$divid,$cssclass);

	/* $map->add_basemap('L.tileLayer',array(
										'//{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=c634f9d387a34d81abf415a54502a92b',
										array(
											'maxZoom' => 19,
											'attribution' => '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
										),
									),'outdoors');
	*/
	$map->add_basemap('L.tileLayer',array(
		'//{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/hybrid.day/{z}/{x}/{y}/{size}/{format}?app_id={app_id}&app_code={app_code}&lg={language}',
		array(
			'attribution'=> 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>',
			'subdomains'=> '1234',
			'mapID'=> 'newest',
			'app_id'=> 'Ej0s9uu65DJYyF9IEjmK',   // Replace with your own developer.here.com app_id
			'app_code'=> 'FJqJ6xYnljndV_wToqpu4A',  // Replace with your own developer.here.com app_code
			'base'=> 'aerial',
			'maxZoom'=> 20,
			'type'=> 'maptile',
			'language'=> 'eng',
			'format'=> 'png8',
			'size'=> '256')
		  ),'aerial');

	$map->add_layer('L.marker',array([$lat,$long]),'startingpoint');

	return $map;
 }


 /**
 * Returns the message body for the password reset mail.
 * Called through the retrieve_password_message filter.
 *
 * @param string  $message    Default mail message.
 * @param string  $key        The activation key.
 * @param string  $user_login The username for the user.
 * @param WP_User $user_data  WP_User object.
 *
 * @return string   The mail message to send.
 */
function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
    // Create new message
    $msg  = __( 'Hello!', 'personalize-login' ) . "\r\n\r\n";
    $msg .= sprintf( __( 'You asked us to reset your password for your account using the email address %s.', 'personalize-login' ), $user_login ) . "\r\n\r\n";
    $msg .= __( "If this was a mistake, or you didn't ask for a password reset, just ignore this email and nothing will happen.", 'personalize-login' ) . "\r\n\r\n";
    $msg .= __( 'To reset your password, visit the following address:', 'personalize-login' ) . "\r\n\r\n";
    $msg .= site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
    $msg .= __( 'Thanks!', 'personalize-login' ) . "\r\n";

    return $msg;
}
//add_filter( 'retrieve_password_message', array( $this, 'replace_retrieve_password_message' ), 10, 4 );



/**
 * Get an s3 instance
 *
 * @return An $s3 api instance
 * @author Andy Walz 9/15/2016
 */
function nr_get_s3_instance( $key = NULL, $secret = NULL, $region = NULL, $streamPrefix = 's3' ){
	// AWS SDK
	require_once 'aws/aws-autoloader.php';

	// Initialize AWS S3 with provided credentials and region
	$aws_key = empty($key) ? 'AKIAIM6MRY37KJT5YW4A' : $key;
	$aws_secret = empty($secret) ? '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' : $secret;

	$region = empty($region) ? 'us-east-2' : $region;

	$s3 = new Aws\S3\S3Client( array(
		'version' => 'latest',
		'region'  => $region,
		'credentials' => array(
			'key'    => $aws_key,
			'secret' => $aws_secret,
		),
	) );

	if( $streamPrefix == 's3' ){
		$s3->registerStreamWrapper(  );
	}else{
		// S3 PHP Stream Wrappers
		// http://docs.aws.amazon.com/aws-sdk-php/v3/guide/service/s3-stream-wrapper.html
		// if we need to interact with two s3 streams, we need to set the prefix on one of them
		\Aws\S3\StreamWrapper::register( $s3, $streamPrefix );
	}

	return $s3;
}

/**
 * Handle force download requests for gallery images
 *
 * @param string $type Type of gallery descriptor (e.g. Itinerary, Destination, Custom, etc.)
 * @param string $urlname Unique gallery identifier used in Canonical URLs for WI's Itineraries, Destinations, etc
 * @param boolean $allowdownload If set to true, a download button appears on hover over each thumbnail in the gallery
 * @param string $singleImageRequest 'all' or number of image to load
 * @return  Custom Envira Gallery HTML based on contents of S3 bucket/prefix
 * @author  Andy Walz 9/15/2016
 */
function nr_get_child_files_from_S3 ( $nrcaid = '02-015' ){

	// S3 down? disable this function:
	// return false;

	// Get list of objects (thumbnails) from S3 bucket/prefix
	$s3Client = nr_get_s3_instance();

	$bucket = 'ngongroad-media';

	$files = array();

	$objects = $s3Client->getIterator('ListObjects', array(
		"Bucket" => $bucket,
		"Prefix" => 'children/' . $nrcaid . '/' //must have the trailing forward slash "/"
	));

	// Query S3 custom metadata for each object and load all info into new images array
	foreach ($objects as $object) {

		$key = $object['Key'];

		// Get paths to full size image and thumbnails
		$filepath = 'https://s3.us-east-2.amazonaws.com/' . $bucket . '/' . $key;

		$files[] = $filepath;

	}

	$html = '';

	if ( empty($files) ) {
		// no gallery or empty gallery found
		$html .= "<span class='not-available-message'>There are no scans at this time for this child, please check back soon.</span>";

	} else {

		rsort( $files );

		// Construct Photo Gallery HTML

		$html .= '<ul class="file-download-list">';
		foreach ( $files as $myfile ) {

			$filename = basename( $myfile, '.pdf' );

			$html .= '<li><a target="_blank" href="' . $myfile . '">' . str_replace( $nrcaid, '', str_replace( '_', ' ', $filename ) ) . '</a></li>';
		}
		$html .= '</ul>';
	}

	return $html;
}
