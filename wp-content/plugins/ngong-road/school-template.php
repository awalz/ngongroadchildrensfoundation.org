<!-- Start Map -->
<div id="schoolmap">
  <?php echo nr_get_map ( 'schoolmap', 'school-map', $school['School_Location__c']['latitude'], $school['School_Location__c']['longitude'], 18); ?>
</div>

<h2><strong>School Name: </strong><?php echo $school['Name']; ?></h2>

<p>
  <strong>Address: </strong> <?php echo $school['School_Address__c']; ?><br />
  <strong>Category: </strong> <?php echo $school['School_Category__c']; ?><br />
  <strong>Level: </strong> <?php echo $school['School_Level__c']. " " . $school['School_Type__c']; ?>
</p>
<p><strong>Description: </strong> <?php echo $school['School_Description__c']; ?></p>

<?php 
if (!empty($school['NgongRoad_org_URL__c'])){
  echo '<a href="'. $school['NgongRoad_org_URL__c'] . '" class="littlebutton bluebutton button">More Info + Photo Gallery</a>';
} 
if (!empty($school['School_Webpage__c'])){
  echo '<a href="'. $school['School_Webpage__c'] . '" class="littlebutton bluebutton button">School Webpage</a>';
} 
?>

<div class="back"><input type="button" value="&laquo; BACK" onclick="history.back(-1)" /></div>
