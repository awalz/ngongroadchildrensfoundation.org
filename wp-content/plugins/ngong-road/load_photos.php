<?php

ini_set('memory_limit','700M');

register_shutdown_function(function(){
	print_r(error_get_last());
});



header("Content-Type: text/plain");
set_time_limit(0);

error_reporting(-1);
ini_set('display_errors', 'On');
ini_set('display_startup_errors', 1);
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true);

require_once(__DIR__ . '/../../../wp-load.php');

set_error_handler(function($error_code){
	$args = func_get_args();
	print $args[1] . ' -- ' . $args[2] . "\n";
	return true;
},E_ALL );

set_exception_handler(function(){
	print_r(func_get_args());
	die("Exception handled");
});

while(@ob_end_flush())

// These files need to be included as dependencies when on the front end.
require_once( ABSPATH . 'wp-admin/includes/image.php' );
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once( ABSPATH . 'wp-admin/includes/media.php' );

// require_once( ABSPATH . 'wp-admin/includes/image.php' );
// require_once( ABSPATH . 'wp-admin/includes/file.php' );
// require_once( ABSPATH . 'wp-admin/includes/media.php' );

// require_once( ABSPATH . 'wp-admin/includes/image.php' );
// require_once( ABSPATH . 'wp-admin/includes/file.php' );
// require_once( ABSPATH . 'wp-admin/includes/media.php' );


//$photo_dir = __DIR__ . '/../../../images/portraits/';
$photo_dir = '/home/ngongroad/www/www/images/portraits/';

$posts = get_posts(array(
	'post_type' => 'children',
	'posts_per_page' => -1,
	'post_status' => 'any',
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'headshot',
			'value' => '',
			'compare' => 'NOT EXISTS',
		),
		array(
			'key' => 'headshot' ,
			'value' => '',
			'compare' => '=',
		)
	)
));

var_dump(count($posts));

foreach($posts as $child){
	$headshot = get_field('headshot',$child->ID);
	$nrca_id = get_field('child_nrca_id',$child->ID);
	$possible_file = $photo_dir . $nrca_id . '.jpg';

	if(empty($nrca_id)){
		continue;
	}

	// If we have a photo...
	if(!empty($headshot)){

		// ... and an nrca_id
		if(!empty($nrca_id)){
			// then set the photos' nrca_id field
			print "Set NRCA ID for ({$child->post_title})'s headshort!\n";
			flush();
			update_field('media_nrca_id',$nrca_id,$headshot['ID']);
		} else {
			// Probably one of the original test children. Should be about 24 of these
			print "Child {$child->ID} ({$child->post_title}) has a headshot but no NRCA id!\n";
			flush();
		}
	} else if(!empty($nrca_id) && file_exists($possible_file)){ // Check for child image

		copy($possible_file,'/tmp/test.jpg');

		if(!file_exists('/tmp/test.jpg')){
			die("no tmp file");
		}

		$files  = array(
			'name' => basename($possible_file),
			'type' => 'image/jpeg',
			'tmp_name' => '/tmp/test.jpg',
			'error' => 0,
			'size' => filesize($possible_file),
		);


		try {
			$post_data = array();
			$overrides = array( 'test_form' => false, 'action' => 'sideload' );

			print "<!-- MMDEBUG: " . __FILE__ . ':' . __LINE__ . "    (" . microtime(TRUE) . ") -->\n";
			$attachment_id = media_handle_sideload($files,0,$files['name']);
			print "<!-- MMDEBUG: " . __FILE__ . ':' . __LINE__ . "    (" . microtime(TRUE) . ") -->\n";

			if(is_wp_error($attachment_id)){
				print "Error loading $possible_file\n";
				flush();
			} else {
				update_field('media_nrca_id',$nrca_id,$attachment_id);
				update_field('headshot',$attachment_id,$child->ID);
				print "Set headshot of {$child->ID} ({$child->post_title}) to $attachment_id\n";
				flush();
			}
		} catch (Exception $e){
			print_r($e);
			die("Something went wrong");
		}

	} else {
		print "No file found at $possible_file\n";
	}
}

@unlink('/tmp/test.jpg');
