<?php
/**
 * Template Name: Featured Image
 *
 * @author Cimbura.com
 * @package Ngong Road
 * @subpackage Cimbura
 */

add_action('genesis_after_entry_content','nr_child_pages',10);
function nr_child_pages() {
  global $post;
  $my_wp_query = new WP_Query('posts_per_page=-1');
  $all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
  $children = get_page_children( $post->ID, $all_wp_pages );
  //echo $post->ID;
  //var_dump($children);
  //var_dump($all_wp_pages);
}

genesis();
