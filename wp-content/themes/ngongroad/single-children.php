<?php

add_filter( 'genesis_post_info', 'ngr_remove_author_from_children',20,10);
function ngr_remove_author_from_children($post_info) {
    $post_info = do_shortcode('[post_date][post_edit]');
    return ''; //$post_info;
}

function get_sponsor_link($post_id = NULL){
    global $post;

    $child_id = (is_null($post_id) ? $post->ID : $post_id);
    $enrollment = str_replace("_", " ", get_post_meta( $child_id, 'enrollment_type_current_year', true) );

    switch ( $enrollment ) {
        case 'Boarding School':
            $req = 1250;
            break;
        case 'Post Secondary':
            $req = 1000;
            break;
        case 'Trade School':
            $req = 1000;
            break;
        default:
            $req = 750;
            break;
    }
    //print $enrollment; die();
    $sponsor_link = '<div class="brownlink sponsorme"><a href="/sponsorship-checkout/?type=New&child_id=' . $child_id . '&req=' . $req . '&enrollment=' . urlencode ( $enrollment ) . '">Sponsor Me!</a></div>';
    return $sponsor_link;
}

add_action('genesis_entry_header',function(){
    print get_sponsor_link();
},11);

add_action( 'genesis_entry_content', function(){
    global $post;
    $headshot = get_field('headshot');

	if ( is_numeric( $headshot ) ) {
		$headshot = array('ID' => $headshot );
	}

	$headshot_src = wp_get_attachment_image_src( $headshot['ID'], 'medium' );

	if ( !$headshot_src ) {
		$headshot_url = '//ngongroad.org/wp-content/uploads/2016/07/icon-profile.png';
	} else {
		$headshot_url = current( $headshot_src );
	}

    print '<div class="inline-headshot"><img src="' . $headshot_url . '" alt="' . htmlentities($post->post_title) . '"></div>';
},8);

add_action('genesis_entry_content',function(){
    global $post;
    $slideshow = get_field('child_photos');
    /*
    $html = '<div class="slick_carousel_centered">';
    foreach($slideshow as $slide){
        $html .= '<div class="oneslide"><img src="' . $slide['sizes']['large'] . '"></div>';
    }
    $html .= '</div>';

     */

	if(!empty($slideshow)){
		$html = '<div class="child-photos">';
		foreach($slideshow as $slide){
			$html .= '<div class="oneslide"><img src="' . $slide['sizes']['medium'] . '"></div>';
		}
		$html .= '</div>';
	} else {
		$html = '';
	}

    $html .= get_sponsor_link();

    print $html;
},11);

genesis();
?>
