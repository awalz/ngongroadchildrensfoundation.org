jQuery('document').ready(function(){
    /*
     jQuery('.sponsorship_type input,.payment_frequency input').on('change',function(){
        var inputname = jQuery('.sponsorship_type input').first().attr('name');
        var freqname = jQuery('.payment_frequency input').first().attr('name');

        var checkedprice = jQuery('input[name='+inputname+']:checked');
        var checkedfreq = jQuery('input[name=' + freqname + ']:checked'); 

        if(checkedfreq.length === 0 || checkedprice.length === 0){
            return;
        }

        var price = checkedprice.val().split('|')[1];
        var frequency = checkedfreq.val();

        jQuery('.payment_frequency li').each(function(){    
            list = jQuery(this);
            var interval = list.find('input').attr('value');

            if(interval == 'Annual'){
                list.find('.total').html('($' + price + ')');
                if(frequency == 'Annual'){
                    ngr_set_sponsorship_total(price);
                }
            }else if(interval == 'Monthly'){
                var thisprice = Math.round(price/12);
                list.find('.total').html('($' + thisprice + ')');
                if(frequency == 'Monthly'){
                    ngr_set_sponsorship_total(thisprice);
                }
            }
        });
    });
    */
});

function ngr_set_sponsorship_total(amt){
    jQuery('.actual_payment_amount span.ginput_total').html('$' + amt);
    jQuery('.actual_payment_amount input').val(amt);
}
