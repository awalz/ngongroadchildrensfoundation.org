<?php

//add sponsor news at the top of the page
add_action( 'genesis_after_entry_content', 'nr_sponsor_news');
function nr_sponsor_news() {

	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		echo '<p>See anything incorrect? Missing? Please let us know via <a href="https://ngongroad.org/feedback-form/?name='.$current_user->user_name.'&email='.$current_user->user_email.'" target="_blank">this form.</a></p>';

		$threemonthsago = getdate(strtotime('11 months ago'));

		$news = new WP_Query( array(
			'post_type' => 'news',
			'date_query' => array(
				array(
					'after' => array(
						'year' => $threemonthsago['year'],
						'month' => $threemonthsago['mon'],
						'day' => $threemonthsago['mday'],
					)
				)),
		));

		if ( $news->have_posts() ) {

			echo '<div class="sponsor-news">';
			//echo '<h2 class="sponsor-news-title">Sponsor Announcements</h2>';
			echo '<div class="one-half first">';
			echo '<h3>Announcements</h3>';
			echo '<ul class="sponsor-news-list">';

			while ( $news->have_posts() ) {
				$news->the_post();
				$id = get_the_id();
				echo '<li class="sponsor-news-list-item">' . get_the_date() . ' - <a href="' . get_the_permalink( $id ) . '">' . get_the_title( $id ) . ' &raquo;</a>';
				//echo get_the_excerpt();
				echo '</li>';
			}

			wp_reset_postdata();
			echo '</ul>';
			echo '</div>';

			echo '<div class="one-half"><h3>Latest News</h3>';

			$latestnews = new WP_Query( array( 'posts_per_page' => 3 ) );

			if ( $latestnews->have_posts() ) {
				echo '<ul class="sponsor-news-list">';

				while ( $latestnews->have_posts() ) {
					$latestnews->the_post();
					$id = get_the_id();
					echo '<li class="sponsor-news-list-item">' . get_the_date() . ' - <a href="' . get_the_permalink( $id ) . '">' . get_the_title( $id ) . ' &raquo;</a>';
					//echo get_the_excerpt();
					echo '</li>';
				}

				wp_reset_postdata();
				echo '</ul>';
			}
			echo '</div>';

			echo '</div>'; // end sponsor-news
		} else {
			// no posts found
		}

	} else {

		$content = '<h4>Sponsor Portal Login:</h4>';
		$args    = array(
			'echo'           => false,
			'redirect'       => site_url( '/sponsor-portal/' ),
			'form_id'        => 'loginform-custom',
			'label_username' => __( 'Email Address' ),

			//'label_password' => __( 'Password' ),
			//'label_remember' => __( 'Remember Me custom text' ),
			'label_log_in'   => __( 'Log In &raquo;' ),
			'remember'       => true
		);
		$content .= wp_login_form( $args );
		$content .= '<hr />';
		$content .= '<a href=' . wp_lostpassword_url( get_permalink() ) . ' title="Lost Password">Lost Your Password?</a> | ';
		$content .= '<a href=' . wp_lostpassword_url( get_permalink() ) . ' title="Lost Password">New User?</a>';

		echo $content;

	}

}

//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'altitude_enqueue_altitude_script' );
function altitude_enqueue_altitude_script() {
	wp_enqueue_script( 'accordion-script', get_bloginfo( 'stylesheet_directory' ) . '/js/accordion.js', array( 'jquery' ), '1.0.0', true );
}

// Retrieve children that are sponsored by this sponsor
add_action( 'genesis_after_entry_content', 'nr_sponsor_children');
function nr_sponsor_children() {

	

	$sponsorid = get_current_user_id();

	if ( isset($_REQUEST['id']) ) {
		// Display a specific child based on their NRCA ID for data clean-up and debugging
		$args = array(
			'post_type' => 'children',
			'meta_key' => 'child_nrca_id',
			'meta_value' => $_REQUEST['id'],
		);
	} else {

		// 1 to 1 relationship query
		// $args = array(
		// 	'post_type' => 'children',
		// 	'meta_key' => 'child_sponsor',
		// 	'meta_value' => $sponsorid,
		// );

		// Many to 1 relationship query
		$args = array(
			'post_type' => 'children',
			'posts_per_page' => 20,
			'meta_query' => array(
				'relation'     => 'AND',
				'sponsor_clause' => array(
					'key'     => 'sponsors',
					'value'   => '"' . $sponsorid . '"',
					'compare' => 'LIKE'
				),
				'age_clause'  => array(
					'key'     => 'secondary_class_of',
					'compare' => 'EXISTS',
				),
			),
			'orderby'    => array(
				'age_clause'  => 'DESC'
			)
		);
	}

	$children = new WP_Query( $args );
	if ( $children->have_posts() ) {
		echo '<h2 class="sponsor-news-title">Children You Sponsor</h2>';
		echo '<div class="sponsor-children">';


		while( $children->have_posts() ) {
			$children->the_post();
			$birthday = get_field('birthday', false, false);
			$age = date_diff(date_create($birthday), date_create('now'))->y;
			$school = get_field('child_school');
			$thumbnail = get_field('headshot');

			$child = [];
			$child['name'] = get_the_title(  );
			$child['birthday'] = $birthday;
			$child['age'] = $age;
			$child['child_nrca_id'] = get_field('child_nrca_id');
			$child['child_sfuuid'] = get_field('child_sfuuid');
			$child['school'] = get_field('current_school');
			$child['class_level_current_year'] = get_field('class_level_current_year');
			$child['enrollment_type_current_year'] = get_field('enrollment_type_current_year');
			$child['secondary_class_of'] = get_field('secondary_class_of');
			$child['renewal_link'] = get_field('renewal_link');
			$child['status'] = get_field('child_sponsored');
			$child['nickname'] = get_field('nickname');
			$id = get_the_id();

			$sfs = SalesforceSync::get_instance();

			$next = false;
			$last = false;

			// Lookup school
			$school_query = "SELECT Enrolled_in_School_ER__c,Enrollment_Date__c,Enrollment_Year__c,Name,Name_of_Child_Enrolled__c FROM Enrollment_Record__c WHERE NRCA_ID__c = '".$child['child_nrca_id']."' ORDER BY Enrollment_Date__c DESC NULLS LAST LIMIT 1";
			$json = $sfs->rest_query($school_query);
			$schoolname = "";
			$schoolid = "";
			if ( $json['totalSize'] > 0 ) {
				$schoolid = $json['records'][0]['Enrolled_in_School_ER__c'];

				$name_query = "SELECT Name FROM School__c WHERE Id = '". $schoolid ."'";
				$json = $sfs->rest_query($name_query);
				if ( $json['totalSize'] > 0 ) {
					$schoolname = $json['records'][0]['Name'];
				}
			}
			echo '<div class="child">';

			echo '<ul class="tabs-nav" id="nav-' . get_the_id( ) . '">';
			echo '<li><a class="tab-active" href="#tab1-' . get_the_id( ) . '" ><span>' . get_the_title(  ) . '</span></a></li>';
			echo '<li><a href="#tab2-' . get_the_id( ) . '" ><span>Inbox</span></a></li>';
			echo '<li><a href="#tab3-' . get_the_id( ) . '"><span>Photos</span></a></li>';
			echo '</ul>';


			echo '<div class="tabs-stage" id="stage' . get_the_id( ) . '">';
			echo '<div style="display: block;" id="tab1-' . get_the_id( ) . '">';
			echo '<div class="child-profile">';
			echo '<div class="child-upper">';
			echo '<div class="one-half first">';

				echo '<img src="' . $thumbnail['sizes']['medium'] . '" alt="' . htmlentities(get_the_title()) . '" class="child-portrait">';

				
			echo '</div>'; // end one-half first




			echo '<div class="one-half">';
			
				echo '<h2>' . get_the_title(  ) . '</h2>';
				echo '<!-- NRCA ID: ' . $child['child_nrca_id'] . '  |  POST ID: ' . $id . '  |  SF ID: ' . get_field('child_sfuuid',$id) . ' -->';
				echo '<ul>';
				if ( ! empty($child['nickname'] )) {
					echo '<li><strong>Nickname:</strong> ' . $child['nickname'] . '</li>';
				}
				echo '<li><strong>Date of Birth:</strong> ' . date_format( date_create($birthday) , 'F j, Y' ) . ' &nbsp; <strong>Age:</strong> '.  $age .'</li>';
				echo '<li><strong>High School Graduation Year:</strong> ' . $child['secondary_class_of'] . '</li>';
				echo '<li><strong>Status:</strong> ' . $child['status'] . '</li>';
				if ( $child['class_level_current_year'] == "Post Secondary") {
					$label = '<strong><a href="/2015/01/28/post-secondary-program-finalized">Post Secondary Program</a></strong>';
				} else {
					$label = '<strong>Current School</strong>';
					echo '<li><strong>Level:</strong> ' . $child['class_level_current_year'] . ', ' . str_replace('_', ' ', $child['enrollment_type_current_year'] ) . '</li>';
				}
				if ( !empty( $schoolname ) ){
					echo '<li>' . $label . ': <a href="/sponsor-portal/school/?Id=' . $schoolid . '" class="school-link">' . $schoolname . '</a></li>';
				} else if ( !empty( $school ) ) {
					echo '<li>'.$label.': <a href="' . get_permalink($school[0]->ID ) . '">' . $school[0]->post_title . '</a></li>';
				} else {
					echo '<li>'.$label.': <a href="/school/' . str_replace(' ', '-', $child['school'] ) . '/">' . $child['school'] . '</a>';
				}
				echo '</ul>';


				echo '<div class="sponsorship-wrapper">';
					$userSalesForceID = get_field( 'user_sf_uuid', 'user_' . get_current_user_id() );
					if ( ! empty( $userSalesForceID ) ) {
						$srlink_query = "SELECT Id, Sponsorship_Renewal_URL__c FROM Sponsor_Relationship__c WHERE Sponsored_Child__c = '" . $child['child_sfuuid'] . "' AND Sponsor_Name__c = '" . $userSalesForceID . "'";
						$srjson = $sfs->rest_query( $srlink_query );
					}

				// For Renewals, lookup current amt in SF
				$query = "SELECT npe03__Last_Payment_Date__c,npe03__Next_Payment_Date__c FROM npe03__Recurring_Donation__c WHERE RD_Sponsor_Relationship__c = '" . $srjson['records'][0]['Id'] . "' LIMIT 1";
				$json = $sfs->rest_query( $query );
				if ( $json['totalSize'] > 0 ) {
					$last = strtotime( $json['records'][0]['npe03__Last_Payment_Date__c'] );
					$next = strtotime( $json['records'][0]['npe03__Next_Payment_Date__c'] );
				}

				// If renewal month or renewal past due show renewal button
				if ( ( $next === false or $next < ( time() + (15 * 24 * 60 * 60)) ) && $child['status'] == "Sponsored") {
					// Lookup sponsor relationship url
					if ( !empty($userSalesForceID) ) {

						if ( $srjson['totalSize'] > 0 ) {
							$renewalurl = $srjson['records'][0]['Sponsorship_Renewal_URL__c'];
						} else {
							$renewalurl = $child['renewal_link'];
						}

					} else {
						$renewalurl = $child['renewal_link'];
					}
					echo ' &nbsp;<a href="'. $renewalurl .'" class="littlebutton renew-sponsorbutton button">Renew Sponsorship</a>';
				}

				if ( $last === false || $next === false ) {
					// Not a paying sponsor
				} else {
					// TODO: Fix this, $child['status'] should never be set to 1
					if ($child['status'] == 1) {
						echo "<p>You last renewed " . date('F Y');
					} else {
						echo "<p>You last renewed " . date('F Y',$last);
					}

					if ( $child['status'] == "Sponsored" ) echo "<br />Your next renewal will be " . date('F Y',$next) . "</p>";
				}
				echo '</div>'; // end sponsorship-wrapper



			// File Downloads Section
			echo '<ul class="accordion">';
			
			echo '<li>';
			echo '<a href="" class="toggle"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" class="svg-inline--fa fa-angle-right fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg> Handwritten Letters &amp; Essays</a>';
			echo '<div class="letters-essays accordion-content">';
			echo nr_get_child_files_from_S3 ( $child['child_nrca_id'] );
			echo '</div>';
			echo '</li>';
			
			echo '<li>';
			echo '<a href="" class="toggle"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" class="svg-inline--fa fa-angle-right fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg> Assessments</a>';
			echo '<div class="assessments accordion-content">';
			echo ngong_show_assessment_list($id, $child);
			echo '</div>'; // end assessments
			echo '</li>';
			
			echo '<li>';
			echo '<a href="" class="toggle"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" class="svg-inline--fa fa-angle-right fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg> School Marks</a>';
			echo '<div class="school-marks accordion-content">';
			echo ngong_show_school_marks_list($id, $child);
			echo '</div>'; // school-marks
			echo '</li>';

			echo '</ul>'; // accordion



			/*
			$json = file_get_contents('https://pics.ngongroad.org/api.php?q=');
			$obj = json_decode($json);


			foreach ($obj as $img) {
				if ( strpos ($img, "http") === 0 ){
					echo '<figure class="gallery-item">
			<div class="gallery-icon portrait">';
					echo '<a href="'. $img .'" data-rel="lightbox-df"><img class="attachment-thumbnail size-thumbnail" sizes="(max-width: 150px) 100vw, 150px" src="'. $img .'" /></a></div></figure>';
				} else if ( empty( $image_ids) ) {
					echo '<em>'.$img.'</em>';
				}
			}
			*/

			echo '</div>'; // end one-half
			echo '</div>'; // end child-upper
			echo '</div>'; // end child-profile
			echo '</div>'; // end Name Tab

			echo '<div style="display: none;" id="tab2-' . get_the_id(  ) . '">';
			echo '<div class="child-email-container">';
			echo '<h3>Inbox</h3>';
			echo '<div class="mail-link">';
					echo '<p><strong>Inbox coming soon!</strong><br /> For now, use the buttons below to communicate with your student.</p>';
					echo '<a href="/sponsor/write-your-child/" class="littlebutton yellowbutton button"><span class="dashicons dashicons-edit"></span> Write Your Child</a>';
					global $current_user;
					get_currentuserinfo();
					echo '<a href="mailto:MyStudent@NgongRoad.org?subject=To '.$child['name'].' Sponsored by '.$current_user->display_name.'" class="littlebutton yellowbutton button"><span class="dashicons dashicons-email-alt"></span> Send Email</a>';
				echo '</div>'; // end mail-link
			
			echo '</div>';
			echo '</div>';

			echo '<div style="display: none;" id="tab3-' . get_the_id(  ) . '">';
			echo '<div class="child-photo-container">';
			echo '<h3>Photos</h3>';
			$image_ids = get_field('child_photos', false, false);
				
				if ( !empty( $image_ids ) ) {
					$shortcode = '[gallery ids="' . implode(',', $image_ids) . '"]';
					//$shortcode = '[envira-gallery-dynamic id="custom-001" images="' . implode(',', $image_ids) . '"]';
					echo do_shortcode( $shortcode );
				}
				global $wpdb;
				$photos = $wpdb->get_results(
					"SELECT * FROM tagged_photos WHERE nrca_id = '".$child['child_nrca_id']."'"
				);
				echo '<div id="gallery-'.$child['child_nrca_id'].'" class="gallery galleryid-'.$child['child_nrca_id'].' gallery-columns-3 gallery-size-thumbnail">';
				foreach ( $photos as $photo )
				{
					$webpath = $photo->webpath . $photo->filename;

					if ( strpos ( $webpath , 'http' ) !== 0 ) {
						$img = 'https://pics.ngongroad.org/' . $webpath;
					} else {
						$img = $webpath;
					}

					echo '<figure class="gallery-item">
								<div class="gallery-icon portrait">';
					echo '<a href="'. $img .'" data-rel="lightbox-df"><img class="attachment-thumbnail size-thumbnail" sizes="(max-width: 150px) 100vw, 150px" src="'. $img .'" /></a></div></figure>';
				}

				echo '</div>'; // end #gallery
				echo '</div>'; // end .child-photo-container
			echo '</div>'; // end tab3

			echo '</div>'; // end tabs-stage
			echo '</div>'; // end Child

			echo '<script>jQuery(document).ready(function() {    

				jQuery(".tabs-nav#nav-' . get_the_id(  ) . ' a").on("click", function (event) {
					event.preventDefault();
					
					jQuery(".tabs-nav#nav-' . get_the_id(  ) . ' .tab-active").removeClass("tab-active");
					jQuery(this).parent().addClass("tab-active");
					jQuery(".tabs-stage#stage' . get_the_id(  ) . ' > div").hide();
					jQuery(jQuery(this).attr("href")).show();
				});
				
				jQuery(".tabs-nav#nav-' . get_the_id(  ) . ' a:first").trigger("click"); 
				
				});</script>';
		}

		wp_reset_postdata();
		echo '</div>'; //  end sponsor-children


		wp_loginout( home_url() ); // Display "Log Out" link.

	} else {
		if ( is_user_logged_in() ) {
				$current_user = wp_get_current_user();
				echo '<p>No sponsored children found for <strong><em>' . $current_user->user_login . '</em></strong>. If this is in error, please <a href="mailto:info@ngongroad.org?subject=Problem%20with%20Sponsor%20Portal">let us know</a>.</p>';
				wp_loginout( home_url() ); // Display "Log Out" link.
		}

	}
}
genesis();
