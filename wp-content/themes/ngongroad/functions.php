<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Remove the edit link
add_filter ( 'genesis_edit_post_link' , '__return_false' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'altitude', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'altitude' ) );

//* Add Image upload and Color select to WordPress Theme Customizer
require_once( get_stylesheet_directory() . '/lib/customize.php' );

//* Include Customizer CSS
include_once( get_stylesheet_directory() . '/lib/output.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Altitude Pro Theme' );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/altitude/' );
define( 'CHILD_THEME_VERSION', '1.0.0' );

//* Enqueue scripts and styles
add_action( 'wp_enqueue_scripts', 'altitude_enqueue_scripts_styles' );
function altitude_enqueue_scripts_styles() {

    wp_enqueue_script( 'altitude-global', get_bloginfo( 'stylesheet_directory' ) . '/js/global.js', array( 'jquery' ), '1.0.0' );

    wp_enqueue_style( 'dashicons' );
    wp_enqueue_style( 'altitude-google-fonts', '//fonts.googleapis.com/css?family=Ek+Mukta:200,800', array(), CHILD_THEME_VERSION );
    wp_enqueue_style( 'customcss', get_stylesheet_directory_uri() . '/custom.css');

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add new image sizes
add_image_size( 'featured-page', 1140, 400, TRUE );

//* Add support for 1-column footer widget area
// add_theme_support( 'genesis-footer-widgets', 1 );

//* Add support for footer menu
add_theme_support ( 'genesis-menus' , array ( 'primary' => 'Primary Navigation Menu', 'secondary' => 'Secondary Navigation Menu', 'footer' => 'Footer Navigation Menu' ) );

//* Unregister the header right widget area
unregister_sidebar( 'header-right' );

//* Reposition the primary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

//* Remove output of primary navigation right extras
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

//* Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_header', 'genesis_do_subnav', 5 );

//* Add secondary-nav class if secondary navigation is used
add_filter( 'body_class', 'altitude_secondary_nav_class' );
function altitude_secondary_nav_class( $classes ) {

    $menu_locations = get_theme_mod( 'nav_menu_locations' );

    if ( ! empty( $menu_locations['secondary'] ) ) {
        $classes[] = 'secondary-nav';
    }
    return $classes;

}

//* Remove the post meta function
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );

//* Remove the entry footer markup (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );

//* Remove the entry meta in the entry footer (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//* Unregister layout settings
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

//* Unregister secondary sidebar
unregister_sidebar( 'sidebar-alt' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
    'flex-height'     => true,
    'width'           => 360,
    'height'          => 76,
    'header-selector' => '.site-title a',
    'header-text'     => false,
) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
    'header',
    'nav',
    'subnav',
    'footer-widgets',
    'footer',
) );

//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'altitude_author_box_gravatar' );
function altitude_author_box_gravatar( $size ) {

    return 176;

}

//* Modify the size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'altitude_comments_gravatar' );
function altitude_comments_gravatar( $args ) {

    $args['avatar_size'] = 120;
    return $args;

}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'altitude_remove_comment_form_allowed_tags' );
function altitude_remove_comment_form_allowed_tags( $defaults ) {

    $defaults['comment_field'] = '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun', 'altitude' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>';
    $defaults['comment_notes_after'] = '';
    return $defaults;

}

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Relocate after entry widget
remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area' );
add_action( 'genesis_after_entry', 'genesis_after_entry_widget_area', 5 );

//* Setup widget counts
function altitude_count_widgets( $id ) {
    global $sidebars_widgets;

    if ( isset( $sidebars_widgets[ $id ] ) ) {
        return count( $sidebars_widgets[ $id ] );
    }

}

function altitude_widget_area_class( $id ) {
    $count = altitude_count_widgets( $id );

    $class = '';

    if( $count == 1 ) {
        $class .= ' widget-full';
    } elseif( $count % 3 == 1 ) {
        $class .= ' widget-thirds';
    } elseif( $count % 4 == 1 ) {
        $class .= ' widget-fourths';
    } elseif( $count % 2 == 0 ) {
        $class .= ' widget-halves uneven';
    } else {
        $class .= ' widget-halves';
    }
    return $class;

}

//* Relocate the post info
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
add_action( 'genesis_entry_header', 'genesis_post_info', 5 );

//* Customize the entry meta in the entry header
add_filter( 'genesis_post_info', 'altitude_post_info_filter' );
function altitude_post_info_filter( $post_info ) {

    $post_info = '[post_date format="M d Y"] [post_edit]';
    return $post_info;

}

//* Customize the entry meta in the entry footer
add_filter( 'genesis_post_meta', 'altitude_post_meta_filter' );
function altitude_post_meta_filter( $post_meta ) {

    $post_meta = 'Written by [post_author_posts_link] [post_categories before=" &middot; Categorized: "]  [post_tags before=" &middot; Tagged: "]';
    return $post_meta;

}

//* Register widget areas
genesis_register_sidebar( array(
    'id'          => 'front-page-1',
    'name'        => __( 'Front Page 1', 'altitude' ),
    'description' => __( 'This is the front page 1 section.', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'front-page-2',
    'name'        => __( 'Front Page 2', 'altitude' ),
    'description' => __( 'This is the front page 2 section.', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'front-page-3',
    'name'        => __( 'Front Page 3', 'altitude' ),
    'description' => __( 'This is the front page 3 section.', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'front-page-4',
    'name'        => __( 'Front Page 4', 'altitude' ),
    'description' => __( 'This is the front page 4 section.', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'front-page-5',
    'name'        => __( 'Front Page 5', 'altitude' ),
    'description' => __( 'This is the front page 5 section.', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'front-page-6',
    'name'        => __( 'Front Page 6', 'altitude' ),
    'description' => __( 'This is the front page 6 section.', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'front-page-7',
    'name'        => __( 'Front Page 7', 'altitude' ),
    'description' => __( 'This is the front page 7 section.', 'altitude' ),
) );


/* Custom */

/**
 * Widgetized page notice
 */
add_action('edit_form_after_title', 'altitude_widgetized_message' );

function altitude_widgetized_message() {

	if ( empty( $_GET['post'] ) && empty( $_POST['post_ID'] ) ) {
		return;
	}

    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

    $frontpage_id = get_option('page_on_front');

    if( $post_id == $frontpage_id ){
        echo '<div class="error">
            <p>This is a <b>widgetized</b> page. You can edit the content under <a href="'.admin_url( 'widgets.php' ).'">Appearance: Widgets</a> or the <a href="' . add_query_arg( array( 'url' => urlencode( get_permalink() ) ), admin_url( 'customize.php' ) ) . '">Customizer</a></p>
		</div>';
    }
}

/**
 * Hide editor on front page
 */
add_action( 'admin_head', 'altitude_hide_editor' );

function altitude_hide_editor() {

	$post_id = -1;
	if(isset($_GET['post'])){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	}

    $frontpage_id = get_option('page_on_front');

    if( $post_id == $frontpage_id ){
        remove_post_type_support('page', 'editor');
    }

}

/* Remove spaces between <li> items so we can make them touch each other and not get a space between menu items */
add_filter('wp_nav_menu_items','remove_menu_spaces',10,2);
function remove_menu_spaces($items,$args){
    $items = str_replace("\n","",$items);
    return $items;
}

remove_action( 'genesis_footer', 'genesis_do_footer' );

genesis_register_sidebar( array(
    'id'          => 'footer-1',
    'name'        => __( 'Footer 1', 'altitude' ),
    'description' => __( 'Footer column 1 widget area', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'footer-2',
    'name'        => __( 'Footer 2', 'altitude' ),
    'description' => __( 'Footer column 2 widget area', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'footer-3',
    'name'        => __( 'Footer 3', 'altitude' ),
    'description' => __( 'Footer column 3 widget area', 'altitude' ),
) );
genesis_register_sidebar( array(
    'id'          => 'footer-4',
    'name'        => __( 'Footer 4', 'altitude' ),
    'description' => __( 'Full width footer area below the other 3', 'altitude' ),
) );

add_action('genesis_footer','ngr_footer_widgets');
function ngr_footer_widgets(){
    // Bottom footer area
    echo '<div class="footer-row">';
    genesis_widget_area( 'footer-1', array(
        'before' => '<div id="footer-1" class="one-third first">',
        'after'  => '</div>',
    ) );
    genesis_widget_area( 'footer-2', array(
        'before' => '<div id="footer-2" class="one-third">',
        'after'  => '</div>',
    ) );
    genesis_widget_area( 'footer-3', array(
        'before' => '<div id="footer-3" class="one-third">',
        'after'  => '</div>',
    ) );
    echo '</div>';
    genesis_widget_area( 'footer-4', array(
        'before' => '<div id="footer-4"><div class="wrap">',
        'after'  => '</div></div>',
    ) );
}

//* Hook menu in footer
add_action( 'genesis_footer', 'nr_footer_menu', 10 );
function nr_footer_menu() {
    printf( '<nav %s>', genesis_attr( 'nav-footer' ) );
    wp_nav_menu( array(
        'theme_location' => 'footer',
        'container'      => false,
        'depth'          => 1,
        'fallback_cb'    => false,
        'menu_class'     => 'genesis-nav-menu',
    ) );

    echo '</nav>';
}

add_action( 'genesis_footer', 'nr_footer_creds', 10 );
//* Footer Credits
function nr_footer_creds() {
	$creds = '<div class="credits">&copy ' . date('Y') . ' &middot; Friends of Ngong Road, a 501(c)(3) nonprofit</div>';
	echo $creds;
}

/* Add schools to menu
add_filter('wp_nav_menu_items','add_schools_to_menu',10,2);
function add_schools_to_menu($items,$args){
    global $post;
    if($args->menu->slug != 'schools'){
        return $items;
    }
    $page_id = get_queried_object_id();
    $schools = get_posts(Array('posts_per_page' => -1,'post_type' => 'school','orderby' => 'title', 'order' => 'ASC'));

    foreach($schools as $school_meta){
        $school = get_post($school_meta->ID);
        if ( $school->ID == $page_id ) {
          $current_page = ' current-page';
        } else {
          $current_page = '';
        }
        // $type = get_field('school_type',$school_meta->ID);
        // if ($type == 'primary') ....
        // WP has some custom css classes (submenu?) for submenus that will help is behave consistantly with other menus
        $items .= '<li class="menu-item' . $current_page .'"><a href="'. get_permalink($school->ID) .'">' . htmlentities($school->post_title). '</a></li>';
    }

    return $items;
}*/

// Add Genesis Simple Sidebar support to schools
add_post_type_support( 'school', 'genesis-simple-sidebars' );

add_shortcode('random_child_polaroid','ngr_random_child_polaroid');
function ngr_random_child_polaroid(){
    //$children = get_posts(Array('post_type' => 'children','orderby' => 'rand','posts_per_page' => 1));
    $children = get_posts(Array(
        'post_type' => 'children',
        //'orderby' => 'rand',
        'posts_per_page' => -1,
        'meta_query' => array(
                              'relation' => 'AND',
                                                    array(
                                                      'key' => 'child_sponsored',
                                                        'value'    => array( 'Waiting for Sponsor', 'Grant Sponsored' ),
                                                        'compare' => 'IN',
                                                    ),
                                                    array(
                                                          'key' => 'enrollment_type_current_year',
                                                          'value'    => array( 'Primary Boarding School', 'Primary Day School', 'Day School', 'Day_School' ),
                                                            'compare' => 'IN',
                                                        )
        ),
    ));
    // $children = get_posts(Array(
    //               'post_type' => 'children',
    //               'orderby' => 'rand',
    //               'posts_per_page' => 1,
    //               'meta_query' => array(
    //                                 array(
    //                                     'key' => 'child_sponsored',
    //                                     'value' => 1,
    //                                     'compare' => '!='
    //                                 )
    //                             ),
    //             ));
    $now = time();

    $children = array( $children[ array_rand( $children, 1 ) ] );

    foreach($children as $childMeta){
        $child = get_post($childMeta->ID);
        $dateString = get_field('birthday',$childMeta->ID);
        $birthdayRaw = strtotime($dateString);
        $headshot = get_field('headshot',$childMeta->ID);
        $name = htmlentities($child->post_title);
        $link = '<a class="brownlink learnmore" href="'. get_permalink($child->ID) .'">';
    }

    $html = '<div class="polaroid"><figure>';
    $html .= '<a href="' . get_permalink($child->ID) . '"><img src="' . ngr_fix_https_in_urls($headshot['sizes']['medium']) . '" alt="Meet ' . $name . '" width="200" height="200"></a>';
    $html .= '<figcaption class="childprofile">';
    $html .= '<span class="childname">'.$name.'</span>';
    $html .= '<span class="childage">Age ' . floor(($now - $birthdayRaw)/31556926) . ' years</span><br>';
    $html .= '<span class="brownlink"><a class="learnmore" href="'.get_permalink($child->ID).'">Learn More</a></span>';
    $html .= '</figcaption></figure></div>';

    return $html;
}

add_shortcode('three_random_child_polaroid','ngr_three_random_child_polaroid');
function ngr_three_random_child_polaroid(){
    $children = get_posts(Array(
        'post_type' => 'children',
        //'orderby' => 'rand',
        'posts_per_page' => -1,
        'meta_query' => array(
                              'relation' => 'AND',
                                                    array(
                                                      'key' => 'child_sponsored',
                                                        'value'    => array( 'Waiting for Sponsor', 'Grant Sponsored' ),
                                                        'compare' => 'IN',
                                                    ),
                                                    array(
                                                          'key' => 'enrollment_type_current_year',
                                                          'value'    => array( 'Primary Boarding School', 'Primary Day School', 'Day School', 'Day_School' ),
                                                            'compare' => 'IN',
                                                        )
        ),
    ));
    // $children = get_posts(Array(
    //               'post_type' => 'children',
    //               'orderby' => 'rand',
    //               'posts_per_page' => 3,
    //               'meta_query' => array(
    // 								array(
    // 									'key' => 'child_sponsored',
    // 									'value' => 1,
    // 									'compare' => '!='
    // 								)
    // 							),
    //             ));
    $now = time();


    $three_children = array_rand( $children, 3 );

    $html = "<div class='row_of_children'>";

	foreach( $three_children as $child_key ) {

		$childMeta = $children[ $child_key ];

        $headshot = get_field('headshot',$childMeta->ID);
        $headshot = get_field('headshot',$childMeta->ID);
        $headshot = get_field('headshot',$childMeta->ID);

        $child = get_post($childMeta->ID);
        $dateString = get_field('birthday',$childMeta->ID);
        $birthdayRaw = strtotime($dateString);
        $name = htmlentities($child->post_title);
        $link = '<a class="brownlink learnmore" href="'. get_permalink($child->ID) .'">';

        $html .= '<div class="polaroid"><figure>';
        $html .= '<a href="' . get_permalink($child->ID) . '"><img src="' . ngr_fix_https_in_urls($headshot['sizes']['medium']) . '" alt="Meet ' . $name . '" width="200" height="200"></a>';
        $html .= '<figcaption class="childprofile">';
        $html .= '<span class="childname">'.$name.'</span>';
        $html .= '<span class="childage">Age ' . floor(($now - $birthdayRaw)/31556926) . ' years</span>';
        $html .= '<span class="brownlink"><a class="learnmore" href="'.get_permalink($child->ID).'">Learn More</a></span>';
        $html .= '</figcaption></figure></div>';
    }

    return $html.'</div>';
}



/**
 * Gravity Wiz // Gravity Forms // Rounding by Increment
 *
 * Provides a variety of rounding functions for Gravity Form Number fields powered by the CSS class setting for each field. Functions include:
 *
 *  + rounding to an increment            (i.e. increment of 100 would round 1 to 100, 149 to 100, 150 to 200, etc) | class: 'gw-round-100'
 *  + rounding up by an increment         (i.e. increment of 50 would round 1 to 50, 51 to 100, 149 to 150, etc)    | class: 'gw-round-up-50'
 *  + rounding down by an increment       (i.e. increment of 25 would round 1 to 0, 26 to 25, 51 to 50, etc)        | class: 'gw-round-down-25'
 *  + rounding up to a specific minimum   (i.e. min of 50 would round 1 to 50, 51 and greater would not be rounded) | class: 'gw-round-min-50'
 *  + rounding down to a specific maximum (i.e. max of 25 would round 26 to 25, 25 and below would not be rounded)  | class: 'gw-round-max-25'
 *
 * @version   1.0
 * @author    David Smith <david@gravitywiz.com>
 * @license   GPL-2.0+
 * @link      http://gravitywiz.com/rounding-increments-gravity-forms/
 */
class GW_Rounding {

    private static $instance = null;

    protected static $is_script_output = false;

    protected $class_regex = 'gw-round-(\w+)-?(\w+)?';

    public static function get_instance() {
        if( null == self::$instance )
            self::$instance = new self;
        return self::$instance;
    }

    private function __construct( $args = array() ) {

        // make sure we're running the required minimum version of Gravity Forms
        if( ! property_exists( 'GFCommon', 'version' ) || ! version_compare( GFCommon::$version, '1.8', '>=' ) )
            return;

        // time for hooks
        add_filter( 'gform_pre_render',            array( $this, 'prepare_form_and_load_script' ) );
        add_filter( 'gform_register_init_scripts', array( $this, 'add_init_script' ) );
        add_filter( 'gform_enqueue_scripts',       array( $this, 'enqueue_form_scripts' ) );

        add_action( 'gform_pre_submission',     array( $this, 'override_submitted_value' ), 10, 5 );
        add_filter( 'gform_calculation_result', array( $this, 'override_submitted_calculation_value' ), 10, 5 );

    }

    function prepare_form_and_load_script( $form ) {

        if( ! $this->is_applicable_form( $form ) ) {
            return $form;
        }

        if( ! self::$is_script_output ) {
            $this->output_script();
        }

        foreach( $form['fields'] as &$field ) {
            if( preg_match( $this->get_class_regex(), $field['cssClass'] ) ) {
                $field['cssClass'] .= ' gw-rounding';
            }
        }

        return $form;
    }

    function output_script() {
        ?>

        <script type="text/javascript">

            var GWRounding;

            ( function( $ ) {

                GWRounding = function( args ) {

                    var self = this;

                    // copy all args to current object: (list expected props)
                    for( prop in args ) {
                        if( args.hasOwnProperty( prop ) )
                            self[prop] = args[prop];
                    }

                    self.init = function() {

                        self.fieldElems = $( '#gform_wrapper_' + self.formId + ' .gw-rounding' );

                        self.parseElemActions( self.fieldElems );

                        self.bindEvents();

                    }

                    self.parseElemActions = function( elems ) {

                        elems.each( function() {

                            var cssClasses      = $( this ).attr( 'class' ),
                                roundingActions = self.parseActions( cssClasses );

                            $( this ).data( 'gw-rounding', roundingActions );

                        } );

                    }

                    self.parseActions = function( str ) {

                        var matches         = getMatchGroups( String( str ), new RegExp( self.classRegex.replace( /\\/g, '\\' ), 'i' ) ),
                            roundingActions = [];

                        for( var i = 0; i < matches.length; i++ ) {

                            var action      = matches[i][1],
                                actionValue = matches[i][2];

                            if( typeof actionValue == 'undefined' ) {
                                actionValue = action;
                                action = 'round';
                            }

                            var roundingAction = {
                                'action':      action,
                                'actionValue': actionValue
                            };

                            roundingActions.push( roundingAction );

                        }

                        return roundingActions;
                    }

                    self.bindEvents = function() {

                        self.fieldElems.find( 'input' ).each( function() {
                            self.applyRoundingActions( $( this ) );
                        } ).blur( function() {
                            self.applyRoundingActions( $( this ) );
                        } );

                        gform.addFilter( 'gform_calculation_result', function( result, formulaField, formId, calcObj ) {

                            var $input = $( '#input_' + formId + '_' + formulaField.field_id )
                                $field = $input.parents( '.gfield' );

                            if( $field.hasClass( 'gw-rounding' ) ) {
                                result = self.getRoundedValue( $input, result );
                            }

                            return result;
                        } );

                    }

                    self.applyRoundingActions = function( $input ) {
                        $input.val( self.getRoundedValue( $input ) );
                    }

                    self.getRoundedValue = function( $input, value ) {

                        var $field  = $input.parents( '.gfield' ),
                            actions = $field.data( 'gw-rounding' );

                        // allows setting the 'gw-rounding' data for an element to null and it will be reparsed
                        if( actions === null ) {
                            self.parseElemActions( $field );
                            actions = $field.data( 'gw-rounding' );
                        }

                        if( typeof actions == 'undefined' || actions === false || actions.length <= 0 ) {
                            return;
                        }

                        if( typeof value == 'undefined' ) {
                            value = $input.val();
                        }

                        if( value == '' ) {
                            value = '';
                        } else {
                            for( var i = 0; i < actions.length; i++ ) {
                                value = GWRounding.round( value, actions[i].actionValue, actions[i].action );
                            }
                        }

                        return value;
                    }

                    GWRounding.round = function( value, actionValue, action ) {

                        // var interval, base, min, max;

                        // value = parseInt( value );
                        // actionValue = parseInt( actionValue );

                        //Mod to force rounding up to nearest int -Andy Walz 3/15/16
                        // switch( action ) {
                        //     case 'min':
                        //         min = actionValue;
                        //         if( value < min ) {
                        //             value = min;
                        //         }
                        //         break;
                        //     case 'max':
                        //         max = actionValue;
                        //         if( value > max ) {
                        //             value = max;
                        //         }
                        //         break;
                        //     case 'up':
                        //         interval = actionValue;
                        //         base     = Math.ceil( value / interval );
                        //         value    = base * interval;
                        //         break;
                        //     case 'down':
                        //         interval = actionValue;
                        //         base     = Math.floor( value / interval );
                        //         value    = base * interval;
                        //         break;
                        //     default:
                        //         interval = actionValue;
                        //         base     = Math.round( value / interval );
                        //         value    = base * interval;
                        //         break;
                        // }

                        return Math.round( value );
                    }

                    self.init();

                }

            } )( jQuery );

        </script>

        <?php

        self::$is_script_output = true;

    }

    function add_init_script( $form ) {

        if( ! $this->is_applicable_form( $form ) ) {
            return;
        }

        $args = array(
            'formId'    => $form['id'],
            'classRegex' => $this->class_regex
        );
        $script = 'new GWRounding( ' . json_encode( $args ) . ' );';

        GFFormDisplay::add_init_script( $form['id'], 'gw_rounding', GFFormDisplay::ON_PAGE_RENDER, $script );

    }

    function enqueue_form_scripts( $form ) {

        if( $this->is_applicable_form( $form ) ) {
            wp_enqueue_script( 'gform_gravityforms' );
        }

    }

    function override_submitted_value( $form ) {

        foreach( $form['fields'] as $field ) {
            if( $this->is_applicable_field( $field ) ) {
                $value = $this->process_rounding_actions( rgpost( "input_{$field['id']}" ), $this->get_rounding_actions( $field ) );
                $_POST[ "input_{$field['id']}" ] = $value;
            }
        }

    }

    function override_submitted_calculation_value( $result, $formula, $field, $form, $lead ) {

        if( $this->is_applicable_field( $field ) ) {
            $result = $this->process_rounding_actions( $result, $this->get_rounding_actions( $field ) );
        }

        return $result;
    }

    function process_rounding_actions( $value, $actions ) {

        foreach( $actions as $action ) {
            $value = $this->round( $value, $action['action'], $action['action_value'] );
        }

        return $value;
    }

    function round( $value, $action, $action_value ) {

        // $value = intval( $value );
        // $action_value = intval( $action_value );

        //Mod to force rounding up to nearest int -Andy Walz 3/15/16
        // switch( $action ) {
        //     case 'min':
        //         $min = $action_value;
        //         if( $value < $min ) {
        //             $value = $min;
        //         }
        //         break;
        //     case 'max':
        //         $max = $action_value;
        //         if( $value > $max ) {
        //             $value = $max;
        //         }
        //         break;
        //     case 'up':
        //         $interval = $action_value;
        //         $base     = ceil( $value / $interval );
        //         $value    = $base * $interval + 1;
        //         break;
        //     case 'down':
        //         $interval = $action_value;
        //         $base     = floor( $value / $interval );
        //         $value    = $base * $interval;
        //         break;
        //     default:
        //         $interval = $action_value;
        //         $base     = round( $value / $interval );
        //         $value    = $base * $interval;
        //         break;
        // }

        return round( $value );
    }

    // # HELPERS

    function is_applicable_form( $form ) {

        foreach( $form['fields'] as $field ) {
            if( $this->is_applicable_field( $field ) ) {
                return true;
            }
        }

        return false;
    }

    function is_applicable_field( $field ) {
        return preg_match( $this->get_class_regex(), rgar( $field, 'cssClass' ) ) == true;
    }

    function get_class_regex() {
        return "/{$this->class_regex}/";
    }

    function get_rounding_actions( $field ) {

        $actions = array();

        preg_match_all( $this->get_class_regex(), rgar( $field, 'cssClass' ), $matches, PREG_SET_ORDER );

        foreach( $matches as $match ) {

            list( $full_match, $action, $action_value ) = array_pad( $match, 3, false );

            if( $action_value === false ) {
                $action_value = $action;
                $action = 'round';
            }

            $action = array(
                'action'       => $action,
                'action_value' => $action_value
            );

            $actions[] = $action;

        }

        return $actions;
    }

}

function gw_rounding() {
    return GW_Rounding::get_instance();
}

gw_rounding();
