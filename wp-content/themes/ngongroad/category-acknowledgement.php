<?php
/**
 * Template Name: My Acknowledgements Template
 *
 * @author Cimbura.com
 * @package Ngong Road
 * @subpackage Cimbura
 */

// You can remove most of the things you're looking at getting rid of just by the appropriate actions from hooks. On ngongroad in the adminbar there's a link to simply show hooks, clicking on that will let you see what hooks are in use and where

remove_action('genesis_entry_header', 'genesis_post_info', 5 );


genesis(); // This function adds in all the default genesis hooks and layout